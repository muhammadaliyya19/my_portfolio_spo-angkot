$(document).ready( function () {
	$('#tabelTrayek').DataTable();
} );
$(function() {
	$('.custom-file-input').on('change',function (){
		let fileName = $(this).val().split('\\').pop();
		$('.custom-file-label').addClass("selected").html(fileName);
	});

	$('#select_kode_trayek').on('change',function (){
		let kode = $(this).val();
		console.log(kode)
		$.ajax({
			url: 'http://localhost/spo-angkot/angkot/getTrayekById',
			data: { id_trayek: kode },
			method: 'post',
			dataType: 'json',
			success: function (data) {
				$('#warna').val(data.warna_angkot);
				$('#jurusan_angkot').val(data.jurusan);				
			}
		});
	});

	$('#tabelTrayek').on("click", ".cekTrayek", function (e){
		e.preventDefault();
		let id_trayek = $(this).data('id');
		// console.log(id_trayek);
		$.ajax({
			url: 'http://localhost/spo-angkot/angkot/get_trayek_route',
			data: { id_trayek: id_trayek },
			method: 'post',
			dataType: 'json',
			success: function (data) {
				$('#holder_rute').html("");
				$('.nama_trayek').html(data['trayek']['jurusan']);
				$('.jarak_trayek').html(data['trayek']['jarak'] + ' Km');
				for (var i = 0; i < data['rute'].length; i++) {
					// console.log(data['rute'][i]);
					$('#holder_rute').append(									
						'<li class="list-group-item">'+data['rute'][i]['nama_jalan']+'</li>'
						);
				}
			}
		});
	});
	
	$('#list_angkot').on("click", ".detail_angkot", function (e){
		e.preventDefault();
		let id_angkot = $(this).data('angkot');
		console.log(id_angkot);
		$.ajax({
			url: 'http://localhost/spo-angkot/angkot/getAngkotById',
			data: { id_angkot: id_angkot },
			method: 'post',
			dataType: 'json',
			success: function (data) {
				console.log(data);
				$('#holder_rute').html("");
				$('.nopol_angkot').html(data['angkot']['nopol']);
				$('.warna_angkot').html(data['angkot']['warna_angkot']);
				$('.jurusan_angkot').html("Jurusan : " + data['angkot']['jurusan'] + " <br>( Jarak Tempuh : " + data['angkot']['jarak'] +" Km)");
				for (var i = 0; i < data['rute'].length; i++) {
					// console.log(data['rute'][i]);
					$('#holder_rute').append(									
						'<li class="list-group-item">'+data['rute'][i]['nama_jalan']+'</li>'
						);
				}
			}
		});
	});

	$('#titik_naik').on('change', function () {
		var titikNaik = $(this).val();
		var titikTurun = $('#titik_turun').val();
		console.log("naik : " + titikNaik + " turun : " + titikTurun);
		if (titikNaik != 'null' && titikTurun != 'null') {
			$.ajax({
				url: 'http://localhost/spo-angkot/angkot/getAngkotByNaikTurun',
				data: { titik_naik: titikNaik, titik_turun: titikTurun },
				method: 'post',
				dataType: 'json',
				success: function (data) {
					console.log(data);
					$('#holder_list_angkot').html("");					
					for (var i = 0; i < data.length; i++) {
						console.log(data['rute'][i]);
						$('#holder_list_angkot').append(									
							"<tr>"+
							"<td>"+(i+1)+"</td>"+
							"<td>"+data[i]['kode_trayek']+"</td>"+
							"<td>"+data[i]['nopol']+"</td>"+
							"<td>"+
							"<a href=\"#\" data-angkot="+data[i]['id_angkot']+" data-xangkot="+data[i]['x_pos']+" data-yangkot="+data[i]['y_pos']+" class=\"btn btn-sm btn-outline-info lokasi_angkot\"><i class=\"fas fa-map-marker-alt\"></i></a>"+
							"<a href=\"#\" data-angkot="+data[i]['id_angkot']+" class=\"btn btn-sm btn-outline-primary detail_angkot\" data-toggle=\"modal\" data-target=\"#modal-detail-angkot\"><i class=\"fas fa-eye\"></i></a>"+
							"</td>"+
							"</tr>"
							);
					}
				}
			});
		}else{
			alert("Titik naik / turun belum dipilih !");
		}
	});	

	$('#titik_turun').on('change', function () {
		var titikTurun = $(this).val();
		var titikNaik = $('#titik_naik').val();
		console.log("naik : " + titikNaik + " turun : " + titikTurun);
		if (titikNaik != 'null' && titikTurun != 'null') {			
			$.ajax({
				url: 'http://localhost/spo-angkot/angkot/getAngkotByNaikTurun',
				data: { titik_naik: titikNaik, titik_turun: titikTurun },
				method: 'post',
				dataType: 'json',
				success: function (data) {
					console.log(data);
					$('#holder_list_angkot').html("");					
					for (var i = 0; i < data.length; i++) {
						// console.log(data['rute'][i]);
						$('#holder_list_angkot').append(									
							"<tr>"+
							"<td>"+(i+1)+"</td>"+
							"<td>"+data[i]['kode_trayek']+"</td>"+
							"<td>"+data[i]['nopol']+"</td>"+
							"<td>"+
							"<a href=\"#\" data-angkot="+data[i]['id_angkot']+" data-xangkot="+data[i]['x_pos']+" data-yangkot="+data[i]['y_pos']+" class=\"btn btn-sm btn-outline-info lokasi_angkot\"><i class=\"fas fa-map-marker-alt\"></i></a>"+
							"<a href=\"#\" data-angkot="+data[i]['id_angkot']+" class=\"btn btn-sm btn-outline-primary detail_angkot\" data-toggle=\"modal\" data-target=\"#modal-detail-angkot\"><i class=\"fas fa-eye\"></i></a>"+
							"</td>"+
							"</tr>"
							);
					}
				}
			});
		}else{
			alert("Titik naik / turun belum dipilih !");
		}		
	});	

});