$(function() {	
		let scanner = new Instascan.Scanner({ video: document.getElementById('preview') });
		scanner.addListener('scan', function (content) {
			console.log(content);
			$('#scanres').val(content);
			getTransfer(content);
		});
		Instascan.Camera.getCameras().then(function (cameras) {
			if (cameras.length > 0) {
				scanner.start(cameras[0]);
			} else {
				console.error('No cameras found.');
			}
		}).catch(function (e) {
			console.error(e);
		});

	$('#camera_check').on('change', function () {
		let cameras = Instascan.Camera.getCameras();		
		if($(this).is(':checked')){
		    $("#cam_text_status").html('On');  // checked
			scanner.start(cameras[0]);
		}else{
		    $("#cam_text_status").html('Off');  // checked
			scanner.stop(cameras[0]);
		}
	});
	
	function getTransfer(qrcode) {
		let qris_in = qrcode;
		$.ajax({
			url: 'http://localhost/spo-angkot/user/getAccountJson',
			data: { qris: qris_in },
			method: 'post',
			dataType: 'json',
			success: function (data) {
				console.log(data);
				var distance = 3;
				// SET VALUE
				$('#scanres').val(data.account.qris_code);
				$('#siangkot').val(data.angkot.nopol + ' - ' + data.angkot.warna_angkot);
				$('#driver').val(data.user.name);
				$('#bayar').val(distance * 1500);

				// SET HTML
				$('.qrdest').html(data.account.qris_code);
				$('.driver').html(data.user.name);
				$('.ang').html(data.angkot.nopol + ' - ' + data.angkot.warna_angkot);	
				// $("#form_naik").submit();
				// console.log("end of payscript");
				var current_title = $(document).attr('title');
				// console.log(current_title);
				if (current_title != "Scan Turun") {
					document.getElementById("form_naik").submit();									
				}else{
					alert("Biaya perjalanan berhasil dihitung! Silakan cek rincian pembayaran sebelum konfirmasi!");
				}
			}
		});	
	}

	$('#titik_naik').on('change', function () {
		var titikNaik = $(this).val();
		var titikTurun = $('#titik_turun').val();
		var count = 0;
		console.log(titikNaik + ' ' + titikTurun);
		count = Math.abs(titikTurun-titikNaik);			
		$('#bayar').val(count * 1500);
	});	

	$('#titik_turun').on('change', function () {
		var titikTurun = $(this).val();
		var titikNaik = $('#titik_naik').val();
		var count = 0;
		console.log(titikNaik + ' ' + titikTurun);
		count = Math.abs(titikTurun-titikNaik);			
		$('#bayar').val(count * 1500);
	});	
});	