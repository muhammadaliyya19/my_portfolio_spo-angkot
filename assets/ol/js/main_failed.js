window.onload = init();
function init() {

  // ====================================================
  // ====================================================
  // SETTER GETTER MAPS VARIABLE

  // COBA EMPTY Source, layer, icon, map and view.
  
  var Koordinat_def = getCoordinates();

  var vectorSource = new ol.source.Vector({
    features: Koordinat_def,
  });

  var vectorLayer = new ol.layer.Vector({
    source: vectorSource,
  });

  var rasterLayer = new ol.layer.Tile({
    source: new ol.source.TileJSON({
      url: 'https://a.tiles.mapbox.com/v3/aj.1x1-degrees.json?secure=1',
      crossOrigin: '',
    }),
  });
  
  var view = new ol.View({
    center: ol.proj.fromLonLat([0, 0]),
    zoom: 2,
  });

  var map = new ol.Map({
    layers: [
    new ol.layer.Tile({
      source: new ol.source.OSM(),
    }), vectorLayer ],
    target: 'map',
    view: view,
  });

  var geolocation = new ol.Geolocation({
    // enableHighAccuracy must be set to true to have the heading value.
    trackingOptions: {
      enableHighAccuracy: true,
    },
    projection: view.getProjection(),
  });

// Ambil Koordinat Driver
function getCoordinates() {
  // Ambil di db, pakai ajax
  var driver_coordinates = [];
  $.ajax({
    url: 'http://localhost/spo-angkot/driver/getAllDriverPosition',
    method: 'post',
    dataType: 'json',
    success: function (data) {
      //   $.each(data.POI, function() {
      //       var pointFeatures = [];
      //       var px = this.x_pos;
      //       var py = this.y_pos;
      //   // Create a lonlat instance and transform it to the map projection.
      //   var lonlat = new ol.LonLat(px, py);
      //   lonlat.transform(new ol.Projection("EPSG:4326"), new ol.Projection("EPSG:900913"));

      //   var pointGeometry = new ol.Geometry.Point(lonlat.lon, lonlat.lat);
      //   var pointFeature = new ol.Feature.Vector(pointGeometry, null, {
      //     pointRadius: 6,
      //     fillOpacity: 0.7,
      //     externalGraphic: 'marker.png',
      //   });

      //   Koordinat_def.push(pointFeature);
      //   vectorLayer.addFeatures(pointFeatures);
      // });
    }    
  });
  var london = new ol.Feature({
    geometry: new ol.geom.Point(ol.proj.fromLonLat([112.28, -7.74626])),
  });

  var paris = new ol.Feature({
    geometry: new ol.geom.Point(ol.proj.fromLonLat([112.28, -7.7478])),
  });
  var berlin = new ol.Feature({
    geometry: new ol.geom.Point(ol.proj.fromLonLat([112.281, -7.75178])),
  });  

  london.setStyle(
    new ol.style.Style({
      image: new ol.style.Circle({
        radius: 6,
        fill: new ol.style.Fill({
          color: '#f54242',
        }),
        stroke: new ol.style.Stroke({
          color: '#fff',
          width: 2,
        }),
      }),
    })
    );

  paris.setStyle(
    new ol.style.Style({
      image: new ol.style.Circle({
        radius: 6,
        fill: new ol.style.Fill({
          color: '#f54242',
        }),
        stroke: new ol.style.Stroke({
          color: '#fff',
          width: 2,
        }),
      }),
    })
    );

  berlin.setStyle(
    new ol.style.Style({
      image: new ol.style.Circle({
        radius: 6,
        fill: new ol.style.Fill({
          color: '#f54242',
        }),
        stroke: new ol.style.Stroke({
          color: '#fff',
          width: 2,
        }),
      }),  
    })
    );

  var driver_coordinates = [london, paris, berlin];
  console.log(driver_coordinates);

  return driver_coordinates;
  // var london = new ol.Feature({
  //       geometry: new ol.geom.Point(ol.proj.fromLonLat([-0.12755, 51.507222])),
  //     });

  //     var paris = new ol.Feature({
  //       geometry: new ol.geom.Point(ol.proj.fromLonLat([2.353, 48.8566])),
  //     });
  //     var berlin = new ol.Feature({
  //       geometry: new ol.geom.Point(ol.proj.fromLonLat([13.3884, 52.5169])),
  //     });  

  
}

// GETTER FUNCTION
// FUNGSI Getter view
function getView() {
  return view;
}

// FUNGSI Getter map 
function getMap() {
  return map;
}

// FUNGSI Getter geoloc
function getGeolocation() {
  return geolocation;
}

// update the HTML page when the position changes.
getGeolocation().on('change', function () {
  console.log(geolocation.position_);  
  var loc = geolocation.position_;
  CenterMap(loc[0], loc[1], map);
  // console.log(geolocation.getProjection());    
});

// handle geolocation error.
getGeolocation().on('error', function (error) {
  var info = document.getElementById('info');
  info.innerHTML = error.message;
  info.style.display = '';
});

var accuracyFeature = new ol.Feature();
geolocation.on('change:accuracyGeometry', function () {
  accuracyFeature.setGeometry(geolocation.getAccuracyGeometry());
});

var positionFeature = new ol.Feature();
positionFeature.setStyle(
  new ol.style.Style({
    image: new ol.style.Circle({
      radius: 6,
      fill: new ol.style.Fill({
        color: '#3399CC',
      }),
      stroke: new ol.style.Stroke({
        color: '#fff',
        width: 2,
      }),
    }),
  })
  );

getGeolocation().on('change:position', function () {
  var coordinates = geolocation.getPosition();
  positionFeature.setGeometry(coordinates ? new ol.geom.Point(coordinates) : null);
});

new ol.layer.Vector({
  map: map,
  source: new ol.source.Vector({
    features: [accuracyFeature, positionFeature],
  }),
});

  // ==============BATAS Var dan fungsi map==============
  // ====================================================
  // ====================================================

// SEPARATED FUNCTION
function CenterMap(long, lat, map) {
  console.log("Long: " + long + " Lat: " + lat);
  map.getView().setCenter(ol.proj.transform([long, lat], 'EPSG:4326', 'EPSG:3857'));
  map.getView().setZoom(17);
}

function el(id) {
  return document.getElementById(id);
}

el('track').addEventListener('change', function () {
  getGeolocation().setTracking(this.checked);
});

}
