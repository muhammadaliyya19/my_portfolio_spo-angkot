// import Map from 'ol/Map';
// import View from 'ol/View';
// import TileLayer from 'ol/layer/Tile';
// import OSM from 'ol/source/OSM';
window.onload = init();

function init() {
	// let map = new ol.Map({
	// 	target: 'map',
	// 	layers: [
	// 	new ol.layer.Tile({
	// 		source: new ol.source.OSM()
	// 	})
	// 	],
	// 	view: new ol.View({
	// 		center: ol.proj.fromLonLat([100.41, 1.82]),
	// 		zoom: 15
	// 	})
	// });


	const washingtonLonLat = [-77.036667, 38.895];
	const map = new ol.Map({
		layers: [
		new ol.layer.Tile({
			source: new ol.source.OSM()
		})
		],
		target: 'map',
		view: new ol.View({
			center: ol.proj.fromLonLat(washingtonLonLat),
			zoom: 12
		})
	});
}