window.onload = init();
function init() {

  var vectorSource = new ol.source.Vector({
     // empty vector
   })


//create the style
var iconStyle = new ol.style.Style({  
  image: new ol.style.Circle({
    radius: 6,
    fill: new ol.style.Fill({
      color: '#f54242',
    }),
    stroke: new ol.style.Stroke({
      color: '#fff',
      width: 2,
    }),
  }),
});

// and apply a style to whole layer
var vectorLayer = new ol.layer.Vector({
  source: vectorSource,
  style: iconStyle
});

var view = new ol.View({
  center: ol.proj.fromLonLat([0, 0]),
  zoom: 2,
});

var map = new ol.Map({
  layers: [new ol.layer.Tile({ source: new ol.source.OSM() }), 
  vectorLayer],
  target: document.getElementById('map'),
  view: view,
});

var geolocation = new ol.Geolocation({
    // enableHighAccuracy must be set to true to have the heading value.
    trackingOptions: {
      enableHighAccuracy: true,
    },
    projection: view.getProjection(),
  });


function getView() {
  return view;
}

// FUNGSI Getter map 
function getMap() {
  return map;
}

// FUNGSI Getter geoloc
function getGeolocation() {
  return geolocation;
}

$.ajax({
  url:'http://localhost/spo-angkot/driver/getAllDriverPosition',
  method: 'post',
  dataType: 'json', 
  success:function(response){
    $(document).ready(function(){
      var jsonlen = response.length;
            for (var i=0; i<jsonlen; i++){
                var coordinates = [];
                    coordinates.push(ol.proj.transform([+response[i].y_pos, +response[i].x_pos], 'EPSG:4326', 'EPSG:3857'));
                  var point_geometry = new ol.geom.Point(ol.proj.fromLonLat([response[i].x_pos, response[i].y_pos]));
                  var feature = new ol.Feature({
                    geometry: point_geometry,
                    id: i
                  });
                  vectorSource.addFeature(feature);
                }
              })
  }
})

var geolocation = new ol.Geolocation({
    trackingOptions: {
      enableHighAccuracy: true,
    },
    projection: view.getProjection(),
  });

  getGeolocation().on('change', function () {
    console.log(geolocation.position_);  
    var loc = geolocation.position_;
    CenterMap(loc[0], loc[1], map);
    // console.log(geolocation.getProjection());   
    console.log(geolocation);
    const idUser = $('#track').attr("data-idUser");
    const roleUser = $('#track').attr("data-roleUser");
    if (roleUser == 2) {
      $.ajax({
        url:'http://localhost/spo-angkot/driver/setDriverPosition',
        data: { x_pos: loc[0], y_pos: loc[1], id_driver: idUser, role: roleUser},
        method: 'post',
        dataType: 'json', 
        success:function(response){
          console.log(response);
        }
      });    
      console.log('eksekusi ajax beres');
    }   
  });

getGeolocation().on('error', function (error) {
  var info = document.getElementById('info');
  info.innerHTML = error.message;
  info.style.display = '';
});

var accuracyFeature = new ol.Feature();
geolocation.on('change:accuracyGeometry', function () {
  accuracyFeature.setGeometry(geolocation.getAccuracyGeometry());
});

var positionFeature = new ol.Feature();
positionFeature.setStyle(
  new ol.style.Style({
    image: new ol.style.Circle({
      radius: 6,
      fill: new ol.style.Fill({
        color: '#3399CC',
      }),
      stroke: new ol.style.Stroke({
        color: '#fff',
        width: 2,
      }),
    }),
  })
  );

getGeolocation().on('change:position', function () {
  var coordinates = geolocation.getPosition();
  positionFeature.setGeometry(coordinates ? new ol.geom.Point(coordinates) : null);
});

new ol.layer.Vector({
  map: map,
  source: new ol.source.Vector({
    features: [accuracyFeature, positionFeature],
  }),
});

function CenterMap(long, lat, map) {
  map.getView().setCenter(ol.proj.transform([long, lat], 'EPSG:4326', 'EPSG:3857'));
  map.getView().setZoom(17);
}

function el(id) {
  return document.getElementById(id);
}

$('#track').on('change', function () {
  getGeolocation().setTracking(this.checked);  
});

$('#select_trayek').on('change',function (){
  let idtrayek = $(this).val();
  if (idtrayek == 'null') {
    $('#holder_list_angkot').html("");
    alert("Menampilkan lokasi semua angkot.");  
    $.ajax({
      url: 'http://localhost/spo-angkot/angkot/getAllAngkot',
      dataType: 'json',
      success: function (data) {
        $('#holder_list_angkot').html("");
        if (data.length > 0) {
          console.log(data);
          for (var i = 0; i < data.length; i++) {
            $('#holder_list_angkot').append(                  
              '<tr>'+
              '<td>'+(i+1)+'</td>'+
              '<td>'+data[i]['kode_trayek']+'</td>'+
              '<td>'+data[i]['nopol']+'</td>'+
              '<td><a href="#" data-angkot="'+data[i]['id']+'" class="btn btn-sm btn-outline-primary"><i class="fas fa-eye"></i></a></td>'+
              '</tr>'
              );
          }        
        }else{
          alert("Angkot tidak ditemukan!");
        }
      }
    });
    // In the maps marker
    $.ajax({
      url:'http://localhost/spo-angkot/driver/getAllDriverPosition',
      method: 'post',
      dataType: 'json', 
      success:function(response){
        $(document).ready(function(){
          var jsonlen = response.length;
          for (var i=0; i<jsonlen; i++){
          var coordinates = [];
            coordinates.push(ol.proj.transform([+response[i].y_pos, +response[i].x_pos], 'EPSG:4326', 'EPSG:3857'));
            var point_geometry = new ol.geom.Point(ol.proj.fromLonLat([response[i].x_pos, response[i].y_pos]));
            var feature = new ol.Feature({
              geometry: point_geometry,
              id: i
            });
            vectorSource.addFeature(feature);
          }
        })
      }
    });
  }else{
    $.ajax({
      url: 'http://localhost/spo-angkot/angkot/getAngkotByTrayek',
      data: { id_trayek: idtrayek },
      method: 'post',
      dataType: 'json',
      success: function (data) {
        $('#holder_list_angkot').html("");
        if (data.length > 0) {
          console.log(data);
          for (var i = 0; i < data.length; i++) {
            $('#holder_list_angkot').append(                  
              '<tr>'+
              '<td>'+(i+1)+'</td>'+
              '<td>'+data[i]['kode_trayek']+'</td>'+
              '<td>'+data[i]['nopol']+'</td>'+
              '<td><a href="#" data-angkot="'+data[i]['id']+'" class="btn btn-sm btn-outline-primary"><i class="fas fa-eye"></i></a></td>'+
              '</tr>'
              );
          }        
        }else{
          alert("Angkot tidak ditemukan!");
        }
      }
    });
  }
});

$('body').on("click", ".lokasi_angkot", function (e){
  e.preventDefault();
  let x_angkot = $(this).data('xangkot');
  let y_angkot = $(this).data('yangkot');
  console.log("pos x angkot" + x_angkot);
  console.log("pos y angkot" + y_angkot);
  CenterMap(x_angkot, y_angkot, map);
});

}
