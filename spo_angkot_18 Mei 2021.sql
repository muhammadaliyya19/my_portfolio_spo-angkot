-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 18 Bulan Mei 2021 pada 09.39
-- Versi server: 10.4.11-MariaDB
-- Versi PHP: 7.4.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `spo_angkot`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `angkot`
--

CREATE TABLE `angkot` (
  `id` int(11) NOT NULL,
  `nama` varchar(64) NOT NULL,
  `nopol` varchar(32) NOT NULL,
  `kode_trayek` varchar(16) NOT NULL,
  `sopir` int(2) NOT NULL,
  `gambar` varchar(128) NOT NULL,
  `created_at` date NOT NULL DEFAULT current_timestamp(),
  `updated_at` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `angkot`
--

INSERT INTO `angkot` (`id`, `nama`, `nopol`, `kode_trayek`, `sopir`, `gambar`, `created_at`, `updated_at`) VALUES
(1, 'Angkot 1', 'AG 12345 EE', 'ABC', 1, 'ang.jpg', '0000-00-00', '2021-05-08'),
(2, 'Angkot 2', 'AG 25345 EE', 'ABC', 2, 'ang.jpg', '2021-05-08', '2021-05-08');

-- --------------------------------------------------------

--
-- Struktur dari tabel `riwayat_perjalanan`
--

CREATE TABLE `riwayat_perjalanan` (
  `id` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `id_angkot` int(11) NOT NULL,
  `rute` int(11) NOT NULL,
  `created_at` date NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `trayek`
--

CREATE TABLE `trayek` (
  `id_trayek` int(11) NOT NULL,
  `kode_trayek` varchar(16) NOT NULL,
  `warna_angkot` varchar(24) NOT NULL,
  `jurusan` text NOT NULL,
  `jarak` float NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `trayek`
--

INSERT INTO `trayek` (`id_trayek`, `kode_trayek`, `warna_angkot`, `jurusan`, `jarak`) VALUES
(1, '01.A', 'HIJAU KUNING', 'ABDUL MUIS-CICAHEUM (via BINONG)', 16.3),
(2, '01.B', 'HIJAU MERAH HATI', 'ABDUL MUIS-CICAHEUM (via ACEH)', 11.55),
(3, '2', 'HIJAU ORANGE', 'ABDUL MUIS-DAGO', 9.3),
(4, '3', 'HIJAU BIRU', 'ABDUL MUIS-LEDENG', 16),
(5, '4', 'ORANGE HIJAU STRIP PUTIH', 'ABDUL MUIS-ELANG', 9.75),
(6, '5', 'HIJAU HITAM', 'CICAHEUM-LEDENG', 14.25),
(7, '6', 'HIJAU ORANGE', 'CICAHEUM-CIROYOM', 17),
(8, '7', 'COKLAT CREAM', 'CICAHEUM-CIWASTRA-DERWATI', 17),
(9, '8', 'MERAH PUTIH', 'CICAHEUM-CIBADUYUT', 16.1),
(10, '9', 'HIJAU ORANGE', 'STASIUN HALL-DAGO', 10),
(11, '10', 'HIJAU KUNING', 'SADANG SERANG-CIROYOM', 11),
(12, '11.A', 'HIJAU BIRU STRIP PUTIH', 'STASIUN HALL-CIMBELEUIT (via EYCKMAN)', 9.8),
(13, '11.B', 'HIJAU BIRU', 'STASIUN HALL-CIMBELEUIT (via CIHAMPELAS)', 8.3),
(14, '12', 'HIJAU MUDA HIJAU STRIP M', 'STASIUN HALL-GEDE BAGE', 21),
(15, '13', 'BIRU MUDA HIJAU STRIP BI', 'STASIUN HALL-SARIJADI', 10.2),
(16, '14', 'BIRU MUDA HIJAU STRIP OR', 'STASIUN HALL-GUNUNG BATU', 8.5),
(17, '15', 'BIRU MUDA KUNING', 'MARGAHAYU RAYA-LEDENG', 19.8),
(18, '16', 'PUTIH HIJAU STRIP KUNING', 'DAGO-RIUNG', 20.6),
(19, '17', 'ORANGE PUTIH HIJAU ', 'PASAR INDUKCARINGIN-DAGO', 19.85),
(20, '18', 'PUTIH KUNING HIJAU STRIP', 'PANGHEGAR PERMAI-DIPATIUKUR', 19.35),
(21, '19.A', 'HIJAU CREAM', 'CIROYOM-SARIJADI (via SUKAJADI)', 11.75),
(22, '19.B', 'HIJAU CREAM', 'CIROYOM-SARIJADI (via SETRASARI MALL)', 10.75),
(23, '20', 'HIJAU STRIP CREAM', 'CIROYOM-BUMI ASRI', 8.35),
(24, '21', 'KUNING PUTIH HIJAU', 'CIROYOM-CIKUDAPATEUH', 12.9),
(25, '22', 'BIRU HIJAU STRIP PUTIH', 'SEDERHANA-CIPAGALO', 16.05),
(26, '23', 'HIJAU STRIP ORANGE', 'SEDERHANA-CIJERAH', 8.9),
(27, '24', 'BIRU MUDA HIJAU STRIP BI', 'SEDERHANA-CIMINDI', 9),
(28, '25', 'ABU-ABU CREAM', 'CIWASTRA-UJUNG BERUNG', 13.4),
(29, '26', 'UNGU HIJAU STRIP PUTIH', 'CISITU-TEGALEGA', 13.95),
(30, '27', 'ABU-ABU HIJAU STRIP PUTI', 'CIJERAH-CIWASTRA-DERWATI', 22.3),
(31, '28', 'BIRU CREAM HIJAU', 'ELANG-GEDE BAGE-UJUNG BERUNG', 22.45);

-- --------------------------------------------------------

--
-- Struktur dari tabel `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `email` varchar(128) NOT NULL,
  `password` varchar(225) NOT NULL,
  `phone` varchar(15) NOT NULL,
  `address` varchar(225) NOT NULL,
  `role` int(2) NOT NULL,
  `is_active` int(1) NOT NULL,
  `created_at` date NOT NULL DEFAULT current_timestamp(),
  `updated_at` date NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `user`
--

INSERT INTO `user` (`id`, `name`, `email`, `password`, `phone`, `address`, `role`, `is_active`, `created_at`, `updated_at`) VALUES
(1, 'Muhammad Aliyya Ilmi', 'muhammadaliyya19@gmail.com', '$2y$10$eAsnE61N6eE5ZfrAi5t0Uepw9bIvMxsSECs7xL8OZjnil9cKwbbLO', '085784114468', 'Kandangan Kediri', 1, 0, '2021-05-03', '2021-05-03'),
(5, 'Dustin Jack', 'dustinjack21@yahoo.co.id', '$2y$10$CnSwONuW366K./tTRqa/p.86TF6hn7etuszDCqUsKC.glrDkdVsb2', '081234567890', 'Kandangan Kediri', 1, 1, '2021-05-03', '2021-05-03'),
(6, 'M. Aliyya Ilmi', 'aliyyailmi20@gmail.com', '$2y$10$CnSwONuW366K./tTRqa/p.86TF6hn7etuszDCqUsKC.glrDkdVsb2', '085789115564', 'Kandangan Kediri', 2, 1, '2021-05-03', '2021-05-03'),
(7, 'Dustin Jack', 'aliyyailmi20@student.ub.ac.id', '$2y$10$CnSwONuW366K./tTRqa/p.86TF6hn7etuszDCqUsKC.glrDkdVsb2', '085784114468', 'Kandangan Kediri', 1, 1, '2021-05-03', '2021-05-03');

-- --------------------------------------------------------

--
-- Struktur dari tabel `user_account`
--

CREATE TABLE `user_account` (
  `id` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `qris_code` varchar(128) NOT NULL,
  `qris_image` varchar(128) NOT NULL,
  `balance` int(11) NOT NULL,
  `created_at` date NOT NULL DEFAULT current_timestamp(),
  `updated_at` date NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `user_account`
--

INSERT INTO `user_account` (`id`, `id_user`, `qris_code`, `qris_image`, `balance`, `created_at`, `updated_at`) VALUES
(1, 1, 'Muhammad Aliyya Ilmi085784114468', 'Muhammad Aliyya Ilmi085784114468.png', 45500, '0000-00-00', '0000-00-00'),
(2, 5, 'Dustin Jack081234567890', 'Dustin Jack081234567890.png', 54500, '0000-00-00', '0000-00-00'),
(3, 6, '085789115564M. Aliyya Ilmi', '085789115564M. Aliyya Ilmi.png', 50000, '2021-05-03', '2021-05-03'),
(4, 7, '085784114468Dustin Jack', '085784114468Dustin Jack.png', 50000, '2021-05-03', '2021-05-03');

-- --------------------------------------------------------

--
-- Struktur dari tabel `user_token`
--

CREATE TABLE `user_token` (
  `id` int(11) NOT NULL,
  `email` varchar(128) NOT NULL,
  `token` varchar(128) NOT NULL,
  `date_created` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `angkot`
--
ALTER TABLE `angkot`
  ADD PRIMARY KEY (`id`),
  ADD KEY `rute` (`kode_trayek`,`sopir`);

--
-- Indeks untuk tabel `riwayat_perjalanan`
--
ALTER TABLE `riwayat_perjalanan`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_user` (`id_user`,`id_angkot`,`rute`);

--
-- Indeks untuk tabel `trayek`
--
ALTER TABLE `trayek`
  ADD PRIMARY KEY (`id_trayek`);

--
-- Indeks untuk tabel `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `role` (`role`);

--
-- Indeks untuk tabel `user_account`
--
ALTER TABLE `user_account`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_user` (`id_user`);

--
-- Indeks untuk tabel `user_token`
--
ALTER TABLE `user_token`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `angkot`
--
ALTER TABLE `angkot`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `riwayat_perjalanan`
--
ALTER TABLE `riwayat_perjalanan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `trayek`
--
ALTER TABLE `trayek`
  MODIFY `id_trayek` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT untuk tabel `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT untuk tabel `user_account`
--
ALTER TABLE `user_account`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `user_token`
--
ALTER TABLE `user_token`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
