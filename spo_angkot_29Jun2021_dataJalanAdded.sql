-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 29 Jun 2021 pada 15.18
-- Versi server: 10.4.11-MariaDB
-- Versi PHP: 7.4.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `spo_angkot`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `angkot`
--

CREATE TABLE `angkot` (
  `id` int(11) NOT NULL,
  `nopol` varchar(32) NOT NULL,
  `id_trayek` int(11) NOT NULL,
  `sopir` int(11) NOT NULL,
  `created_at` date NOT NULL DEFAULT current_timestamp(),
  `updated_at` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `angkot`
--

INSERT INTO `angkot` (`id`, `nopol`, `id_trayek`, `sopir`, `created_at`, `updated_at`) VALUES
(1, 'D 123 EA', 2, 1, '2021-05-30', '2021-05-30'),
(2, 'AG 320 AA', 2, 3, '2021-06-16', '2021-06-16'),
(3, 'D 123 EA', 3, 1, '2021-05-30', '2021-05-30'),
(4, 'AG 320 AA', 4, 3, '2021-06-16', '2021-06-16'),
(5, 'D 123 EA', 6, 1, '2021-05-30', '2021-05-30'),
(6, 'AG 320 AA', 7, 3, '2021-06-16', '2021-06-16'),
(7, 'D 123 EA', 8, 1, '2021-05-30', '2021-05-30'),
(8, 'AG 320 AA', 9, 3, '2021-06-16', '2021-06-16');

-- --------------------------------------------------------

--
-- Struktur dari tabel `driver_pos`
--

CREATE TABLE `driver_pos` (
  `id` int(11) NOT NULL,
  `id_driver` int(11) NOT NULL,
  `x_pos` float NOT NULL,
  `y_pos` float NOT NULL,
  `updated_at` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `driver_pos`
--

INSERT INTO `driver_pos` (`id`, `id_driver`, `x_pos`, `y_pos`, `updated_at`) VALUES
(1, 1, 112.279, -7.74709, '2021-06-21'),
(2, 3, 0, 0, '2021-06-16');

-- --------------------------------------------------------

--
-- Struktur dari tabel `jalan`
--

CREATE TABLE `jalan` (
  `id_jalan` int(3) NOT NULL,
  `nama_jalan` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `jalan`
--

INSERT INTO `jalan` (`id_jalan`, `nama_jalan`) VALUES
(1, 'Terminal Kebon Kelapa'),
(2, 'Jl Pungkur'),
(3, 'Jl Karapitan'),
(4, 'Jl Buah Batu'),
(5, 'Jl Banteng'),
(6, 'Jl Sancang'),
(7, 'Jl Lodaya'),
(8, 'Jl Martanegara'),
(9, 'Jl Turangga'),
(10, 'Jl Gatot Subroto'),
(11, 'BSM'),
(12, 'Binong'),
(13, 'Jl Krara Condong'),
(14, 'Jl Jakarta'),
(15, 'Jl WR Supratman'),
(16, 'Jl Katamso'),
(17, 'Jl Pahlawan'),
(18, 'Jl Cikutra'),
(19, 'Jl PHH Mustofa (Suci)'),
(20, 'Terminal Cicaheum'),
(21, 'Jl Dewi Sartika'),
(22, 'Jl Kautamaan lstri'),
(23, 'Jl Balong Gede'),
(24, 'Jl Sunda'),
(25, 'Jl Lombok'),
(26, 'Jl Aceh'),
(27, 'Jl Taman Pramuka'),
(28, 'Jl Cendana'),
(29, 'Taman WR Supratman'),
(30, 'Jl Sumbawa'),
(31, 'Jl Belitung'),
(32, 'Jl Sumatera'),
(33, 'Jl Sulawesi'),
(34, 'Jl Seram'),
(35, 'Jl RE Martadinata (Riau)'),
(36, 'Jl lr H Juanda (Dago)'),
(37, 'RS Boromeus (Dago)'),
(38, '\"ITB (Jl Ganesha'),
(39, 'Simpang Dago'),
(40, 'Terminal Dago'),
(41, 'Jl Banda'),
(42, 'BIP (Dago)'),
(43, 'Jl Merdeka'),
(44, 'Jl Wastu Kencana'),
(45, 'Jl Rivai'),
(46, 'Jl Cipaganti'),
(47, 'Jl Setiabudi'),
(48, 'Jl Karang Sari'),
(49, 'Jl Sukaladi'),
(50, 'Terminal Ledeng'),
(51, 'Jl Diponogoro'),
(52, 'Jl Sulanjana'),
(53, 'Jl Tamansari'),
(54, 'Jl Siliwangi'),
(55, 'Jl Cihampelas'),
(56, 'Jl Lamping'),
(57, 'Jl Sukajadi'),
(58, 'Jl Surapati (Suci)'),
(59, 'Lapangan Gasibu (Surapati)'),
(60, 'Jl Panatayuda'),
(61, 'Jl Dipati Ukuran'),
(62, 'Jl Sumur Bandung'),
(63, 'Jl Eyckman'),
(64, 'RS.Hahasn Sadikin'),
(65, 'Jl Pasir Kaliki'),
(66, 'Jl Pajajaran'),
(67, 'Jl Abdul Rahman Saleh'),
(68, 'Jl Garuda'),
(69, 'Jl Ciroyom'),
(70, 'Terminal Ciroyom'),
(71, 'Stasiun Bandung (Barat)'),
(72, 'Jl Stasiun Timur'),
(73, 'Viaduct'),
(74, 'Jl Perintis Kemerdekaan'),
(75, 'Jl RE Martadinata'),
(76, 'Jl lr H Juanda'),
(77, 'Sirnpang Dago'),
(78, 'Terminal Stasiun'),
(79, 'Jl Suniaraja'),
(80, 'Jl Otista'),
(82, 'Jl Palajaran'),
(83, 'Jl Cihermpelas'),
(84, 'Jl Sederhana'),
(85, 'Jl Sernpuna'),
(86, 'Jl Ciumbuleuit'),
(87, 'UNPAR (Ciumbuleuit)'),
(88, 'Terminal Ciumbuleuit'),
(89, 'Jl Bapa Husen'),
(90, 'RS Hasan Sadikin'),
(91, 'Jl Pasteur'),
(92, 'Jl Cipto'),
(93, 'Jl Cicendo'),
(94, 'Jl Kebon Kawung'),
(95, 'Stasiun Bandung (Kebon Kawung)'),
(96, 'Jl Kebon Jati'),
(97, 'Jl Dulatip'),
(98, 'Pasar Baru'),
(99, 'Jl Otto lskandar dinata (Otista)'),
(100, 'Jl Kepatihan'),
(101, 'Jl Dalem Kaum'),
(102, 'Alun-Alun (Asia Afrika)'),
(103, 'Jl Banceuy'),
(104, 'Jl ABC'),
(105, 'Jl Naripan'),
(106, 'Jl Veteran'),
(107, 'Jl Ahmad Yani'),
(108, 'Jl Burangrang'),
(109, 'Jl Halimun'),
(110, 'Jl Malabar'),
(111, 'Jl Talaga Bodas'),
(112, 'Jl Pelajar Pejuang'),
(113, 'Jl Madanegara'),
(114, 'Jl Reog'),
(115, 'Jl Karawitan'),
(116, 'Jl Kliningan'),
(117, 'Margahayu Raya'),
(118, 'Metro'),
(119, 'Riung Bandung'),
(120, 'Pasar Induk Gede Bage'),
(121, 'Jl Suniaraia'),
(122, 'Jl Kebon JuKut'),
(123, 'lstana Plaza (Pasir Kaliki)'),
(124, 'Jl DR Junjunan (Terusan Pasteur)'),
(125, 'BTC (Pasteur)'),
(126, 'Jl Surya Sumantri'),
(127, 'Universitas Maranatha (Surya Sumantri)'),
(128, 'Sarijadi'),
(129, 'Jl Lemah Nendeut'),
(130, 'Jl Sari Rasa'),
(131, 'Jl Sari Wangi'),
(132, 'Jl Sari Manah'),
(133, 'Jl Sari Asih'),
(134, 'Terminal Sarijadi'),
(135, 'Jl Rum'),
(136, 'Jl Gunawan'),
(137, 'Jl Otten'),
(138, 'BEC'),
(139, 'Jl Westhoff'),
(140, 'Jl Gunung Batu'),
(141, 'Terminal Gunung Batu'),
(142, 'Terminal Margahayu'),
(143, 'Jl Ranca Bolang (Margahayu Raya)'),
(144, 'Jl Kiara Condong'),
(145, 'Jl Dipati Ukur'),
(146, 'Jl Sentot Alibasyah'),
(147, 'Jl Diponegoro'),
(148, 'Jl Citarum'),
(149, 'Jl Laswi'),
(150, 'Jl Sukabumi'),
(151, 'Jl Cipamokolan (Riung Bandung)'),
(152, 'Jl Riung Bandung'),
(153, 'Terminal Riung Bandung'),
(154, 'Jl Cigadung Raya'),
(155, 'Jl Cikutra Barat'),
(156, 'Jl Cikapayang'),
(157, 'Jl Sawunggaling'),
(158, 'Jl Rangga Gading'),
(159, 'UNISBA & UNPAS (Tamansari)'),
(160, 'Jl Purnawarman'),
(161, 'Jl Arjuna'),
(162, 'Jl Supadio'),
(163, 'Jl Rajawali Timur'),
(164, 'Jl Waringrn'),
(165, 'Jl Sudirman'),
(166, 'Jl Jamika'),
(167, 'Jl Terusan Jamika'),
(168, 'Jl Sukamulya'),
(169, 'Jl Sukarno-Hatta'),
(170, 'Jl Babakan Ciparay'),
(171, 'Pasar Induk Carinqin {Sukarno-Hatta)'),
(172, 'Terminal Panghegar'),
(173, 'Jl Cisaranten'),
(174, 'Jl Cicukang'),
(175, 'Jl AH Nasutlon (Raya Ujung Berung)'),
(176, 'Sindanglaya'),
(177, 'Cicadas (Ahmad Yani)'),
(178, 'Jl Ambon'),
(179, 'Masjid lstiqamah'),
(180, 'Jl Cisanggarung'),
(181, 'Jl Cimanuk'),
(182, 'Jl Cimandiri'),
(183, 'Jl Cimalaya'),
(184, 'Jl Ganesha- ITB'),
(185, 'RS Boromeus'),
(186, 'Jl Hasanudin'),
(187, 'Terminal Dipati Ukur'),
(188, 'Jl Baladewa'),
(189, 'Jl Dursasana'),
(190, 'Jl Sindang Sirna'),
(191, 'Jl Geger Kalong Hilir'),
(192, 'Jl SariEndah'),
(193, 'Jl Sari Jadi'),
(194, 'Jl Holis'),
(195, 'Jl Bojong Raya'),
(196, 'Jl Cijerah'),
(197, 'Bumi Asri');

-- --------------------------------------------------------

--
-- Struktur dari tabel `riwayat_perjalanan`
--

CREATE TABLE `riwayat_perjalanan` (
  `id` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `id_angkot` int(11) NOT NULL,
  `biaya` int(5) NOT NULL,
  `created_at` date NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `riwayat_perjalanan`
--

INSERT INTO `riwayat_perjalanan` (`id`, `id_user`, `id_angkot`, `biaya`, `created_at`) VALUES
(12, 5, 1, 13500, '2021-06-29');

-- --------------------------------------------------------

--
-- Struktur dari tabel `rute_trayek`
--

CREATE TABLE `rute_trayek` (
  `id_data` int(4) NOT NULL,
  `count_biaya` int(3) NOT NULL,
  `id_trayek` int(3) NOT NULL,
  `id_jalan` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `rute_trayek`
--

INSERT INTO `rute_trayek` (`id_data`, `count_biaya`, `id_trayek`, `id_jalan`) VALUES
(1, 1, 1, 1),
(2, 2, 1, 2),
(3, 3, 1, 3),
(4, 4, 1, 4),
(5, 5, 1, 5),
(6, 6, 1, 6),
(7, 7, 1, 7),
(8, 8, 1, 8),
(9, 9, 1, 9),
(10, 10, 1, 10),
(11, 11, 1, 11),
(12, 12, 1, 12),
(13, 13, 1, 13),
(14, 14, 1, 14),
(15, 15, 1, 15),
(16, 16, 1, 16),
(17, 17, 1, 17),
(18, 18, 1, 18),
(19, 19, 1, 19),
(20, 20, 1, 20),
(21, 1, 2, 1),
(22, 2, 2, 21),
(23, 3, 2, 22),
(24, 4, 2, 23),
(25, 5, 2, 2),
(26, 6, 2, 3),
(27, 7, 2, 24),
(28, 8, 2, 25),
(29, 9, 2, 26),
(30, 10, 2, 27),
(31, 11, 2, 28),
(32, 12, 2, 29),
(33, 13, 2, 16),
(34, 14, 2, 17),
(35, 15, 2, 18),
(36, 16, 2, 19),
(37, 17, 2, 20),
(38, 1, 3, 1),
(39, 2, 3, 21),
(40, 3, 3, 22),
(41, 4, 3, 23),
(42, 5, 3, 2),
(43, 6, 3, 3),
(44, 7, 3, 24),
(45, 8, 3, 30),
(46, 9, 3, 31),
(47, 10, 3, 32),
(48, 11, 3, 26),
(49, 12, 3, 33),
(50, 13, 3, 34),
(51, 14, 3, 35),
(52, 15, 3, 36),
(53, 16, 3, 37),
(54, 17, 3, 38),
(55, 18, 3, 39),
(56, 19, 3, 40),
(57, 1, 4, 1),
(58, 2, 4, 21),
(59, 3, 4, 22),
(60, 4, 4, 23),
(61, 5, 4, 2),
(62, 6, 4, 3),
(63, 7, 4, 24),
(64, 8, 4, 30),
(65, 9, 4, 25),
(66, 10, 4, 41),
(67, 11, 4, 35),
(68, 12, 4, 42),
(69, 13, 4, 43),
(70, 14, 4, 26),
(71, 15, 4, 44),
(72, 16, 4, 45),
(73, 17, 4, 46),
(74, 18, 4, 47),
(75, 19, 4, 48),
(76, 20, 4, 49),
(77, 21, 4, 47),
(78, 22, 4, 50),
(79, 1, 6, 20),
(80, 2, 6, 19),
(81, 3, 6, 16),
(82, 4, 6, 15),
(83, 5, 6, 51),
(84, 6, 6, 52),
(85, 7, 6, 53),
(86, 8, 6, 54),
(87, 9, 6, 55),
(88, 10, 6, 56),
(89, 11, 6, 46),
(90, 12, 6, 47),
(91, 13, 6, 48),
(92, 14, 6, 57),
(93, 15, 6, 47),
(94, 16, 6, 50),
(95, 1, 7, 20),
(96, 2, 7, 19),
(97, 3, 7, 58),
(98, 4, 7, 59),
(99, 5, 7, 60),
(100, 6, 7, 61),
(101, 7, 7, 39),
(102, 8, 7, 62),
(103, 9, 7, 54),
(104, 10, 7, 55),
(105, 11, 7, 63),
(106, 12, 7, 64),
(107, 13, 7, 65),
(108, 14, 7, 66),
(109, 15, 7, 67),
(110, 16, 7, 68),
(111, 17, 7, 69),
(112, 18, 7, 70),
(113, 1, 10, 71),
(114, 2, 10, 72),
(115, 3, 10, 73),
(116, 4, 10, 74),
(117, 5, 10, 44),
(118, 6, 10, 75),
(119, 7, 10, 76),
(120, 8, 10, 37),
(121, 9, 10, 38),
(122, 10, 10, 77),
(123, 11, 10, 40),
(124, 1, 12, 78),
(125, 2, 12, 79),
(126, 3, 12, 80),
(127, 4, 12, 72),
(128, 5, 12, 73),
(129, 6, 12, 74),
(130, 7, 12, 44),
(131, 8, 12, 82),
(132, 9, 12, 83),
(133, 10, 12, 45),
(134, 11, 12, 46),
(135, 12, 12, 63),
(136, 13, 12, 84),
(137, 14, 12, 85),
(138, 15, 12, 46),
(139, 16, 12, 47),
(140, 17, 12, 86),
(141, 18, 12, 87),
(142, 19, 12, 88),
(143, 1, 13, 88),
(144, 2, 13, 86),
(145, 3, 13, 87),
(146, 4, 13, 55),
(147, 5, 13, 89),
(148, 6, 13, 84),
(149, 7, 13, 65),
(150, 8, 13, 90),
(151, 9, 13, 91),
(152, 10, 13, 55),
(153, 11, 13, 45),
(154, 12, 13, 92),
(155, 13, 13, 66),
(156, 14, 13, 93),
(157, 15, 13, 94),
(158, 16, 13, 95),
(159, 17, 13, 65),
(160, 18, 13, 96),
(161, 19, 13, 78),
(162, 1, 14, 78),
(163, 2, 14, 97),
(164, 3, 14, 98),
(165, 4, 14, 99),
(166, 5, 14, 100),
(167, 6, 14, 21),
(168, 7, 14, 101),
(169, 8, 14, 102),
(170, 9, 14, 103),
(171, 10, 14, 104),
(172, 11, 14, 105),
(173, 12, 14, 24),
(174, 13, 14, 106),
(175, 14, 14, 107),
(176, 15, 14, 10),
(177, 16, 14, 108),
(178, 17, 14, 109),
(179, 18, 14, 110),
(180, 19, 14, 111),
(181, 20, 14, 112),
(182, 21, 14, 113),
(183, 22, 14, 114),
(184, 23, 14, 115),
(185, 24, 14, 116),
(186, 25, 14, 4),
(187, 26, 14, 175),
(188, 27, 14, 117),
(189, 28, 14, 118),
(190, 29, 14, 119),
(191, 30, 14, 120),
(192, 1, 15, 78),
(193, 2, 15, 121),
(194, 3, 15, 80),
(195, 4, 15, 72),
(196, 5, 15, 73),
(197, 6, 15, 122),
(198, 7, 15, 94),
(199, 8, 15, 95),
(200, 9, 15, 65),
(201, 10, 15, 123),
(202, 11, 15, 124),
(203, 12, 15, 125),
(204, 13, 15, 126),
(205, 14, 15, 127),
(206, 15, 15, 128),
(207, 16, 15, 129),
(208, 17, 15, 130),
(209, 18, 15, 131),
(210, 19, 15, 132),
(211, 20, 15, 133),
(212, 21, 15, 134),
(213, 1, 16, 78),
(214, 2, 16, 80),
(215, 3, 16, 72),
(216, 4, 16, 73),
(217, 5, 16, 74),
(218, 6, 16, 44),
(219, 7, 16, 66),
(220, 8, 16, 55),
(221, 9, 16, 45),
(222, 10, 16, 135),
(223, 11, 16, 136),
(224, 12, 16, 137),
(225, 13, 16, 91),
(226, 14, 16, 138),
(227, 15, 16, 139),
(228, 16, 16, 124),
(229, 17, 16, 140),
(230, 18, 16, 141),
(231, 1, 17, 142),
(232, 2, 17, 143),
(233, 3, 17, 175),
(234, 4, 17, 144),
(235, 5, 17, 14),
(236, 6, 17, 15),
(237, 7, 17, 28),
(238, 8, 17, 27),
(239, 9, 17, 75),
(240, 10, 17, 43),
(241, 11, 17, 44),
(242, 12, 17, 66),
(243, 13, 17, 93),
(244, 14, 17, 45),
(245, 15, 17, 46),
(246, 16, 17, 47),
(247, 17, 17, 48),
(248, 18, 17, 57),
(249, 19, 17, 47),
(250, 20, 17, 50),
(251, 1, 18, 40),
(252, 2, 18, 36),
(253, 3, 18, 39),
(254, 4, 18, 145),
(255, 5, 18, 60),
(256, 6, 18, 58),
(257, 7, 18, 146),
(258, 8, 18, 147),
(259, 9, 18, 148),
(260, 10, 18, 75),
(261, 11, 18, 149),
(262, 12, 18, 150),
(263, 13, 18, 107),
(264, 14, 18, 144),
(265, 15, 18, 175),
(266, 16, 18, 117),
(267, 17, 18, 118),
(268, 18, 18, 151),
(269, 19, 18, 152),
(270, 20, 18, 153),
(271, 1, 19, 40),
(272, 2, 19, 154),
(273, 3, 19, 155),
(274, 4, 19, 17),
(275, 5, 19, 58),
(276, 6, 19, 156),
(277, 7, 19, 53),
(278, 8, 19, 157),
(279, 9, 19, 158),
(280, 10, 19, 159),
(281, 11, 19, 53),
(282, 12, 19, 44),
(283, 13, 19, 160),
(284, 14, 19, 82),
(285, 15, 19, 93),
(286, 16, 19, 45),
(287, 17, 19, 65),
(288, 18, 19, 66),
(289, 19, 19, 161),
(290, 20, 19, 162),
(291, 21, 19, 69),
(292, 22, 19, 163),
(293, 23, 19, 96),
(294, 24, 19, 164),
(295, 25, 19, 171),
(296, 26, 19, 166),
(297, 27, 19, 167),
(298, 28, 19, 168),
(299, 29, 19, 175),
(300, 30, 19, 170),
(301, 31, 19, 171),
(302, 1, 20, 172),
(303, 2, 20, 173),
(304, 3, 20, 174),
(305, 4, 20, 175),
(306, 5, 20, 176),
(307, 6, 20, 20),
(308, 7, 20, 107),
(309, 8, 20, 177),
(310, 9, 20, 144),
(311, 10, 20, 14),
(312, 11, 20, 150),
(313, 12, 20, 149),
(314, 13, 20, 75),
(315, 14, 20, 178),
(316, 15, 20, 179),
(317, 16, 20, 180),
(318, 17, 20, 181),
(319, 18, 20, 182),
(320, 19, 20, 183),
(321, 20, 20, 147),
(322, 21, 20, 52),
(323, 22, 20, 53),
(324, 23, 20, 184),
(325, 24, 20, 185),
(326, 25, 20, 186),
(327, 26, 20, 145),
(328, 27, 20, 187),
(329, 1, 21, 70),
(330, 2, 21, 161),
(331, 3, 21, 66),
(332, 4, 21, 188),
(333, 5, 21, 189),
(334, 6, 21, 65),
(335, 7, 21, 57),
(336, 9, 21, 191),
(337, 10, 21, 192),
(338, 11, 21, 193),
(339, 12, 21, 194),
(340, 13, 21, 132),
(341, 14, 21, 131),
(342, 15, 21, 134),
(343, 1, 23, 70),
(344, 2, 23, 69),
(345, 3, 23, 68),
(346, 4, 23, 171),
(347, 5, 23, 175),
(348, 6, 23, 201),
(349, 7, 23, 202),
(350, 8, 23, 203),
(351, 9, 23, 204),
(352, 0, 0, 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `trayek`
--

CREATE TABLE `trayek` (
  `id_trayek` int(11) NOT NULL,
  `kode_trayek` varchar(16) NOT NULL,
  `warna_angkot` varchar(24) NOT NULL,
  `jurusan` text NOT NULL,
  `jarak` float NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `trayek`
--

INSERT INTO `trayek` (`id_trayek`, `kode_trayek`, `warna_angkot`, `jurusan`, `jarak`) VALUES
(1, '01.A', 'HIJAU KUNING', 'ABDUL MUIS-CICAHEUM (via BINONG)', 16.3),
(2, '01.B', 'HIJAU MERAH HATI', 'ABDUL MUIS-CICAHEUM (via ACEH)', 11.55),
(3, '2', 'HIJAU ORANGE', 'ABDUL MUIS-DAGO', 9.3),
(4, '3', 'HIJAU BIRU', 'ABDUL MUIS-LEDENG', 16),
(5, '4', 'ORANGE HIJAU STRIP PUTIH', 'ABDUL MUIS-ELANG', 9.75),
(6, '5', 'HIJAU HITAM', 'CICAHEUM-LEDENG', 14.25),
(7, '6', 'HIJAU ORANGE', 'CICAHEUM-CIROYOM', 17),
(8, '7', 'COKLAT CREAM', 'CICAHEUM-CIWASTRA-DERWATI', 17),
(9, '8', 'MERAH PUTIH', 'CICAHEUM-CIBADUYUT', 16.1),
(10, '9', 'HIJAU ORANGE', 'STASIUN HALL-DAGO', 10),
(11, '10', 'HIJAU KUNING', 'SADANG SERANG-CIROYOM', 11),
(12, '11.A', 'HIJAU BIRU STRIP PUTIH', 'STASIUN HALL-CIMBELEUIT (via EYCKMAN)', 9.8),
(13, '11.B', 'HIJAU BIRU', 'STASIUN HALL-CIMBELEUIT (via CIHAMPELAS)', 8.3),
(14, '12', 'HIJAU MUDA HIJAU STRIP M', 'STASIUN HALL-GEDE BAGE', 21),
(15, '13', 'BIRU MUDA HIJAU STRIP BI', 'STASIUN HALL-SARIJADI', 10.2),
(16, '14', 'BIRU MUDA HIJAU STRIP OR', 'STASIUN HALL-GUNUNG BATU', 8.5),
(17, '15', 'BIRU MUDA KUNING', 'MARGAHAYU RAYA-LEDENG', 19.8),
(18, '16', 'PUTIH HIJAU STRIP KUNING', 'DAGO-RIUNG', 20.6),
(19, '17', 'ORANGE PUTIH HIJAU ', 'DAGO-PASAR INDUKCARINGIN', 19.85),
(20, '18', 'PUTIH KUNING HIJAU STRIP', 'PANGHEGAR PERMAI-DIPATIUKUR', 19.35),
(21, '19.A', 'HIJAU CREAM', 'CIROYOM-SARIJADI (via SUKAJADI)', 11.75),
(22, '19.B', 'HIJAU CREAM', 'CIROYOM-SARIJADI (via SETRASARI MALL)', 10.75),
(23, '20', 'HIJAU STRIP CREAM', 'CIROYOM-BUMI ASRI', 8.35),
(24, '21', 'KUNING PUTIH HIJAU', 'CIROYOM-CIKUDAPATEUH', 12.9),
(25, '22', 'BIRU HIJAU STRIP PUTIH', 'SEDERHANA-CIPAGALO', 16.05),
(26, '23', 'HIJAU STRIP ORANGE', 'SEDERHANA-CIJERAH', 8.9),
(27, '24', 'BIRU MUDA HIJAU STRIP BI', 'SEDERHANA-CIMINDI', 9),
(28, '25', 'ABU-ABU CREAM', 'CIWASTRA-UJUNG BERUNG', 13.4),
(29, '26', 'UNGU HIJAU STRIP PUTIH', 'CISITU-TEGALEGA', 13.95),
(30, '27', 'ABU-ABU HIJAU STRIP PUTI', 'CIJERAH-CIWASTRA-DERWATI', 22.3),
(31, '28', 'BIRU CREAM HIJAU', 'ELANG-GEDE BAGE-UJUNG BERUNG', 22.45);

-- --------------------------------------------------------

--
-- Struktur dari tabel `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `email` varchar(128) NOT NULL,
  `password` varchar(225) NOT NULL,
  `phone` varchar(15) DEFAULT NULL,
  `address` varchar(225) DEFAULT NULL,
  `role` int(2) NOT NULL,
  `is_active` int(1) NOT NULL,
  `created_at` date NOT NULL DEFAULT current_timestamp(),
  `updated_at` date NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `user`
--

INSERT INTO `user` (`id`, `name`, `email`, `password`, `phone`, `address`, `role`, `is_active`, `created_at`, `updated_at`) VALUES
(1, 'Driver Jack', 'muhammadaliyya19@gmail.com', '$2y$10$fghmQ08muqYtXcLKIFEqgOpUkNpABeOei4WvnF..SPOw4iJP652hC', '085784114468', 'Cisitu Indah', 2, 1, '2021-05-30', '2021-05-30'),
(2, 'Passenger Dustin Jack', 'aliyyailmi20@gmail.com', '$2y$10$v7x2sEOAtpyUiNweOjA9CeeInX9/FXDimLIMbLf.dmT10tcyl3RCC', '089789456122', 'Kanadangan', 1, 1, '2021-05-30', '2021-06-28'),
(3, 'Muhammad Aliyya Ilmi', 'aliyyailmi20@student.ub.ac.id', '$2y$10$iJQhwh.cy3bNRqHGsG/acuKKXkn9PNvSfqN97CAn03HwCcuPeUzji', '085784114468', 'Kandangan Kediri', 1, 1, '2021-06-16', '2021-06-16'),
(5, 'Reinhard M', 'dustinjack21@yahoo.co.id', '$2y$10$g4Y5QcWJ5Ris8qJ.M0WE7.eRGH26ByGjkccD72rVkpgALAEbXm1LK', NULL, NULL, 1, 1, '2021-06-28', '2021-06-28');

-- --------------------------------------------------------

--
-- Struktur dari tabel `user_account`
--

CREATE TABLE `user_account` (
  `id` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `qris_code` varchar(128) NOT NULL,
  `qris_image` varchar(128) NOT NULL,
  `balance` int(11) NOT NULL DEFAULT 10000,
  `created_at` date NOT NULL DEFAULT current_timestamp(),
  `updated_at` date NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `user_account`
--

INSERT INTO `user_account` (`id`, `id_user`, `qris_code`, `qris_image`, `balance`, `created_at`, `updated_at`) VALUES
(1, 1, '085784114468Driver Jack', '085784114468Driver Jack.png', 127000, '2021-05-30', '2021-05-30'),
(2, 2, '089789456122Penumpang Dustin', '089789456122Penumpang Dustin.png', 2500, '2021-05-30', '2021-05-30'),
(3, 3, '085784114468Muhammad Aliyya Ilmi', '085784114468Muhammad Aliyya Ilmi.png', 7500, '2021-06-16', '2021-06-16'),
(5, 5, 'Reinhard M', 'Reinhard M.png', 6500, '2021-06-28', '2021-06-28');

-- --------------------------------------------------------

--
-- Struktur dari tabel `user_token`
--

CREATE TABLE `user_token` (
  `id` int(11) NOT NULL,
  `email` varchar(128) NOT NULL,
  `token` varchar(128) NOT NULL,
  `date_created` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `angkot`
--
ALTER TABLE `angkot`
  ADD PRIMARY KEY (`id`),
  ADD KEY `rute` (`id_trayek`,`sopir`),
  ADD KEY `sopir` (`sopir`);

--
-- Indeks untuk tabel `driver_pos`
--
ALTER TABLE `driver_pos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_driver` (`id_driver`);

--
-- Indeks untuk tabel `jalan`
--
ALTER TABLE `jalan`
  ADD PRIMARY KEY (`id_jalan`);

--
-- Indeks untuk tabel `riwayat_perjalanan`
--
ALTER TABLE `riwayat_perjalanan`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_user` (`id_user`,`id_angkot`),
  ADD KEY `id_angkot` (`id_angkot`);

--
-- Indeks untuk tabel `rute_trayek`
--
ALTER TABLE `rute_trayek`
  ADD PRIMARY KEY (`id_data`);

--
-- Indeks untuk tabel `trayek`
--
ALTER TABLE `trayek`
  ADD PRIMARY KEY (`id_trayek`);

--
-- Indeks untuk tabel `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `role` (`role`);

--
-- Indeks untuk tabel `user_account`
--
ALTER TABLE `user_account`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_user` (`id_user`);

--
-- Indeks untuk tabel `user_token`
--
ALTER TABLE `user_token`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `angkot`
--
ALTER TABLE `angkot`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT untuk tabel `driver_pos`
--
ALTER TABLE `driver_pos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `jalan`
--
ALTER TABLE `jalan`
  MODIFY `id_jalan` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=198;

--
-- AUTO_INCREMENT untuk tabel `riwayat_perjalanan`
--
ALTER TABLE `riwayat_perjalanan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT untuk tabel `rute_trayek`
--
ALTER TABLE `rute_trayek`
  MODIFY `id_data` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=353;

--
-- AUTO_INCREMENT untuk tabel `trayek`
--
ALTER TABLE `trayek`
  MODIFY `id_trayek` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT untuk tabel `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT untuk tabel `user_account`
--
ALTER TABLE `user_account`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT untuk tabel `user_token`
--
ALTER TABLE `user_token`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `angkot`
--
ALTER TABLE `angkot`
  ADD CONSTRAINT `angkot_ibfk_1` FOREIGN KEY (`sopir`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `angkot_ibfk_2` FOREIGN KEY (`id_trayek`) REFERENCES `trayek` (`id_trayek`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ketidakleluasaan untuk tabel `driver_pos`
--
ALTER TABLE `driver_pos`
  ADD CONSTRAINT `driver_pos_ibfk_1` FOREIGN KEY (`id_driver`) REFERENCES `user` (`id`);

--
-- Ketidakleluasaan untuk tabel `riwayat_perjalanan`
--
ALTER TABLE `riwayat_perjalanan`
  ADD CONSTRAINT `riwayat_perjalanan_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `riwayat_perjalanan_ibfk_2` FOREIGN KEY (`id_angkot`) REFERENCES `angkot` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ketidakleluasaan untuk tabel `user_account`
--
ALTER TABLE `user_account`
  ADD CONSTRAINT `user_account_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
