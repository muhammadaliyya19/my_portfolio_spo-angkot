###################
Prototype Sistem Pembayaran Online Angkot - Kota Bandung
###################

**Bahasa Pemrograman** : PHP 7

**Framework** : Codeigniter 3.11

**UI Theme** : Kuber - from Colorlib

*********
List Modul
*********

Terdapat dua role user : Driver/Sopir | Passenger/Penumpang

**Untuk Penumpang :**

1. Auth |Login, Logout, Registrasi|
2. Cari Angkot

    - Lacak lokasi saat ini
    - Lacak lokasi angkot terdekat
    - Pilih titik naik dan titik turun
    - Tunggu angkot datang dan kemudian naik angkot
    - Scan QR angkot di titik naik
    - Scan QR angkot di titik turun
    - Konfirmasi pembayaran

3. Info Trayek
4. Profil
    - Edit profil
    - Ubah metode bayar
    - History perjalanan

**Untuk Driver :**

1. Auth |Login, Logout, Registrasi|
2. Cari Angkot

    - Lacak dan update lokasi saya saat ini

3. Info Trayek
4. Profil
    - Edit profil
    - Edit angkot
    - Ubah metode bayar
    - History pendapatan


*********
------ Cara run app local ------
*********

Akun login : silakan registrasi langsung

1. Download atau clone repo ini

2. Buka htdocs dan buat folder bernama "spo-angkot"

3. Extract project didalam folder spo-angkot.

4. Run local server

5. Buat database bernama "spo_angkot_now" dan import file sql yang disediakan

6. Lakukan penyesuaian file database.php pada CodeIgniter jika password, username dan hostname anda tidak default

7. Akses melalui browser dengan membuka localhost/spo-angkot

Terima kasih.
