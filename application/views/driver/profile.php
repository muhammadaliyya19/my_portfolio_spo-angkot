<div class="mt-5 pt-4"></div>
<!-- CONTENT -->
<!-- Start home-about Area -->
<section class="home-about-area" id="home">
	<div class="container">
		<div class="card m-3">
			<div class="card-body">
				<div class="row">
					<div class="col-12">
						<?php echo $this->session->flashdata('message'); ?>
					</div>
				</div>
				<ul class="nav" role="tablist">
					<li class="active">
						<a href="#profile" class="mr-3 btn btn-warning text-dark" aria-controller="profile" role="tab" data-toggle="tab"> Profile </a>
					</li>
					<li>
						<a href="#angkot" class="mr-3 btn btn-warning text-dark" aria-controller="angkot" role="tab" data-toggle="tab"> Angkot </a>
					</li>					
					<li>
						<a href="#payment" class="mr-3 btn btn-warning text-dark" aria-controller="payment" role="tab" data-toggle="tab"> Payment </a>
					</li>
					<li>
						<a href="#revenue" class="mr-3 btn btn-warning text-dark" aria-controller="revenue" role="tab" data-toggle="tab"> Pendapatan </a>
					</li>
				</ul>
				<div class="tab-content">				
					<!-- 1st Data diri/profile -->
					<div class="tab-pane mt-2 active" id="profile">
						<div class="row align-items-center">
							<div class="col-lg-12">
								<div class="row my-2">
									<div class="col-lg-12 mb-2">
										<div class="card card-success card-outline" style="height: 100%;">
											<div class="card-body box-profile">
												<div class="text-center">
													<img class="profile-user-img img-fluid img-circle"
													src="<?=base_url(); ?>assets/img/fav_profile_driver.jpg"
													alt="Profile picture" style="height:200px;">
												</div>

												<h3 class="profile-username text-center mt-2">
													<?=$user['name']; ?>
													<a href="#" data-toggle="modal" data-target="#modal-profil" data-area="<?php ?>" data-id="<?php ?>">
														<i class="fa fa-edit"></i>
													</a>
												</h3>

												<p class="text-muted text-center"><?=$user_role; ?></p>

												<ul class="list-group list-group-unbordered mb-3">
													<li class="list-group-item">
														<b>Email</b> <a class="float-right"><?=$user['email']; ?></a>
													</li>
													<li class="list-group-item">
														<b>Address</b> <a class="float-right"><?=$user['address']; ?></a>
													</li>
													<li class="list-group-item">
														<b>Phone</b> <a class="float-right"><?=$user['phone']; ?></a>
													</li>
													<li class="list-group-item">
														<b>Last Updated</b> <a class="float-right"><?=$user['updated_at']; ?></a>
													</li>
													<li class="list-group-item">
														<b>Join Date</b> <a class="float-right"><?=$user['created_at']; ?></a>
													</li>
												</ul>
											</div>
											<!-- /.card-body -->
										</div>
									</div>
									<!-- PLACEHOLDER EDIT PROFILE -->
									<div class="col">
										
									</div>								
								</div>
							</div>							
						</div>				
					</div>				
					<!-- 2nd Pengaturan angkot -->
					<div class="tab-pane mt-3" id="angkot">
						<div class="row align-items-center">
							<div class="col-lg-12">
								<?php if($angkot != null): ?>
									<div class="row my-2">
										<div class="col-lg-12">
											<div class="card card-success card-outline" style="height: 100%;">
												<div class="card-body box-profile">
													<div class="text-center">
														<img class="profile-user-img img-fluid img-circle"
														src="<?= base_url('assets/img/fav/ang.jpg'); ?>"
														alt="Angkot picture" style="height:200px;">
													</div>

													<h3 class="profile-username text-center mt-2">
														Angkot <?=$user['name']; ?>
														<a href="#" data-toggle="modal" data-target="#modal-angkot" data-area="<?php ?>" data-id="<?php ?>">
															<i class="fa fa-edit"></i>
														</a>
													</h3>
													<p class="text-muted text-center">Kota Bandung</p>
													<ul class="list-group list-group-unbordered mb-3">
														<li class="list-group-item">
															<b>Nopol</b> <a class="float-right"><?=$angkot['nopol']; ?></a>
														</li>
														<li class="list-group-item">
															<b>Kode Trayek</b> <a class="float-right"><?=$angkot['kode_trayek']; ?></a>
														</li>
														<li class="list-group-item">
															<b>Jurusan</b> <a class="float-right"><?=$angkot['jurusan']; ?></a>
														</li>
														<li class="list-group-item">
															<b>Warna</b> <a class="float-right"><?=$angkot['warna_angkot']; ?></a>
														</li>
													</ul>
												</div>
												<!-- /.card-body -->
											</div>
										</div>									
									</div>
								<?php else: ?>
									<div class="alert alert-warning alert-dismissible fade show" role="alert">
										Silakan lengkapi data angkot yang anda kemudikan ! 
										<button type="button" class="close" data-dismiss="alert" aria-label="Close">
											<span aria-hidden="true">&times;</span>
										</button>
									</div>
									<div class="card">
										<div class="card-body">
											<form method="POST" action="<?= base_url('angkot/tambahAngkot'); ?>">
												<input type="hidden" name="sopir" id="sopir" value="<?=$user['id']; ?>">
												<div class="row">
													<div class="col-3 tanggal_label">Nomor Polisi / TNKB <strong>*</strong></div>
													<div class="col-9"><input type="text" name="nopol" id="nopol" class="form-control" placeholder="Plat nomor..."></div>
												</div>
												<div class="row mt-2">
													<div class="col-3 tanggal_label">Kode Trayek Angkot <strong>*</strong></div>
													<div class="col-9">
														<select class="form-control" id="select_kode_trayek" name="trayek">
															<option value="">-- Pilih Kode Trayek Angkot --</option>
															<?php foreach($trayek as $t): ?>
																<option value="<?= $t['id_trayek'];?>"><?=$t['kode_trayek']; ?></option>
															<?php endforeach; ?>
														</select>
													</div>
												</div>
												<div class="row mt-2">
													<div class="col-3 tanggal_label">Warna Angkot</div>
													<div class="col-9">
														<input type="text" disabled="" id="warna" class="form-control" placeholder="Warna...">
													</div>
												</div>
												<div class="row mt-2">
													<div class="col-3 tanggal_label">Jurusan Trayek</div>
													<div class="col-9">
														<textarea disabled="" id="jurusan_angkot" class="form-control" rows="4" placeholder="Jurusan..."></textarea>
													</div>
												</div>
												<div class="row mt-2">
													<div class="col-6 float-left">
														<strong>*</strong> Wajib diisi
													</div>
													<div class="col-6 float-left">
														<button type="submit" class="btn btn-warning text-dark float-right tbhAngkot">Simpan</button>
													</div>
												</div>
											</form>
										</div>
									</div>
								<?php endif; ?>
							</div>
						</div>
					</div>

					<div class="tab-pane mt-3" id="payment">
						<div class="row align-items-center">
							<div class="col-lg-12">
								<div class="row">
									<div class="col-lg-12 col-md-12 col-xs-12">
										<!--  -->
										<div class="row my-2">
											<div class="col-lg-12">
												<div class="card card-success card-outline" style="height: 100%;">
													<div class="card-body box-profile">
														<div class="text-center">
															<img src="<?=$dir;?>" class="thumbnail" alt="Angkot picture" style="height:200px;">
														</div>
														<h3 class="profile-username text-center mt-2">
															Informasi Account
															<a href="#" data-toggle="modal" data-target="#modal-akun" data-area="<?php ?>" data-id="<?php ?>">
																<i class="fa fa-edit"></i>
															</a>
														</h3>

														<p class="text-muted text-center"><?=$user['name']; ?></p>

														<ul class="list-group list-group-unbordered mb-3">
															<li class="list-group-item">
																<b>QR Code</b> <a class="float-right"><?=$account['qris_code']; ?></a>
															</li>
															<li class="list-group-item">
																<b>Saldo</b> <a class="float-right"><?=$account['balance']; ?></a>
															</li>
															<li class="list-group-item">
																<b>Last Transaction</b> <a class="float-right"><?=$account['updated_at']; ?></a>
															</li>
															<li class="list-group-item">
																<b>Last Updated</b> <a class="float-right"><?=$account['updated_at']; ?></a>
															</li>									
														</ul>
													</div>
													<!-- /.card-body -->
												</div>
											</div>									
										</div>
										<!--  -->										
									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="tab-pane mt-3" id="revenue">
						<div class="row align-items-center">
							<div class="col-lg-12">
								<div class="card card-success card-outline" style="overflow-y:scroll;">
									<div class="my-3" align="center">
										<div class="row p-2">
											<div class="col-3 text-left">
												<strong>Last Update : </strong>
												<?php //$total_revenue; ?>
												22-05-2021
											</div>
											<div class="col-6">
												<h3>Pendapatan</h3>
											</div>
											<div class="col-3 text-right">
												<strong>Total : </strong>
												Rp. <?=$total_revenue['total']; ?>,-
											</div>
										</div>
									</div>
									<table class="table table-striped">
										<thead>
											<tr>
												<th scope="col">No.</th>
												<th scope="col">Biaya</th>
												<th scope="col">Tanggal</th>												
											</tr>
										</thead>
										<tfoot>
											<tr>
												<th scope="col">No.</th>
												<th scope="col">Biaya</th>
												<th scope="col">Tanggal</th>												
											</tr>
										</tfoot>
										<tbody>
											<?php $i = 1; foreach ($revenue as $r): ?>
											<tr>
												<td><?= $i; ?></td>
												<td><?= $r['biaya']; ?>,-</td>
												<td><?= $r['created_at']; ?></td>
											</tr>											
											<?php $i++; endforeach; ?>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>
</div>	

<!-- MODAL DIALOG PROFIL -->
<div class="modal fade" id="modal-profil">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Edit Profil</h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body modal-evidence">
				<div class="card" style="height: 100%;">
					<div class="card-body">
						<form action="<?=base_url('driver/edit_profile'); ?>" method="post">
							<div class="row mt-2">
								<div class="col-3">Nama<strong>*</strong></div>
								<div class="col">
									<input type="hidden" name="id_user" class="form-control" value="<?=$user['id']; ?>">
									<input type="text" name="name" class="form-control" placeholder="Nama..." value="<?=$user['name']; ?>">
								</div>
							</div>
							<div class="row mt-2">
								<div class="col-3">E-mail<strong>*</strong></div>
								<div class="col">
									<input type="email" name="email" class="form-control" placeholder="E-mail..." value="<?=$user['email']; ?>">
								</div>
							</div>
							<div class="row mt-2">
								<div class="col-3">Phone<strong>*</strong></div>
								<div class="col">
									<input type="number" name="phone" class="form-control" placeholder="Nomor HP..." value="<?=$user['phone']; ?>">
								</div>
							</div>
							<div class="row mt-2">
								<div class="col-3">Alamat<strong>*</strong></div>
								<div class="col">
									<input type="text" name="address" class="form-control" placeholder="Alamat..." value="<?=$user['address']; ?>">
								</div>
							</div>
							<div class="row mt-2">
								<div class="col-3">Password Saat Ini<strong>*</strong></div>
								<div class="col">
									<input type="password" name="old_password" class="form-control" placeholder="Masukkan password ..." >
								</div>
							</div>
							<div class="row mt-2">                
								<div class="col-3">Password Baru</div>
								<div class="col">
									<input type="password" name="password" class="form-control" placeholder="Password baru ...">
								</div>
								<div class="col">
									<input type="password" name="repassword" class="form-control" placeholder="Konfirmasi password ...">
								</div>
							</div>
							<div class="row mt-2">
								<div class="col">
									<strong>* Wajib diisi</strong>
								</div>
								<div class="col">
									<button type="submit" class="btn btn-warning text-dark float-right ml-2">Simpan</button>
									<a href="#" data-dismiss="modal" aria-label="Close" class="btn btn-secondary float-right">Batal</a>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- MODAL ANGKOT -->
<div class="modal fade" id="modal-angkot">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Edit Data Angkot</h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="card" style="height: 100%;">
					<div class="card-body">
						<form action="<?=base_url('angkot/editAngkot'); ?>" method="post">
							<div class="row mt-2">
								<div class="col-3">Nomor Polisi / TNKB<strong>*</strong></div>
								<div class="col">
									<input type="hidden" name="id_angkot" class="form-control" value="<?=$angkot['id']; ?>">
									<input type="text" name="nopol" class="form-control" placeholder="Plat nomor..." value="<?=$angkot['nopol']; ?>">
								</div>
							</div>
							<div class="row mt-2">
								<div class="col-3 tanggal_label">Kode Trayek Angkot <strong>*</strong></div>
								<div class="col-9">
									<select class="form-control" id="select_kode_trayek" name="trayek">
										<option value="">-- Pilih Kode Trayek Angkot --</option>
										<?php foreach($trayek as $t): ?>
											<option value="<?= $t['id_trayek'];?>" <?php if($angkot['id_trayek'] == $t['id_trayek']){echo "selected";} ?>
												><?=$t['kode_trayek']; ?></option>
											<?php endforeach; ?>
										</select>
									</div>
								</div>
								<div class="row mt-2">
									<div class="col-3 tanggal_label">Warna Angkot</div>
									<div class="col-9">
										<input type="text" disabled="" id="warna" class="form-control" placeholder="Warna..." value="<?=$angkot['warna_angkot']; ?>">
									</div>
								</div>
								<div class="row mt-2">
									<div class="col-3 tanggal_label">Jurusan Trayek</div>
									<div class="col-9">
										<textarea disabled="" id="jurusan_angkot" class="form-control" rows="4" placeholder="Jurusan..."><?=$angkot['jurusan']; ?></textarea>
									</div>
								</div>
								<div class="row mt-2">
									<div class="col">
										<strong>* Wajib diisi</strong>
									</div>
									<div class="col">
										<button type="submit" class="btn btn-warning text-dark float-right ml-2">Simpan</button>
										<a href="#" data-dismiss="modal" aria-label="Close" class="btn btn-secondary float-right">Batal</a>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- MODAL ACCOUNT -->
	<div class="modal fade" id="modal-akun">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title">Edit Data Account</h4>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="card" style="height: 100%;">
						<div class="card-body">
							<form action="" method="post">
								<div class="row mt-2">
									<div class="col-3">Bank / E-Wallet<strong>*</strong></div>
									<div class="col">
										<select name="ewallet" class="form-control">
											<option value="-">--- Pilih ---</option>
											<option value="BNI">BNI</option>
											<option value="BRI">BRI</option>
											<option value="Mandiri">Mandiri</option>
											<option value="OVO">OVO</option>
											<option value="Gopay">Gopay</option>
											<option value="Shopee Pay">Shopee Pay</option>
										</select>
									</div>
								</div>
								<div class="row mt-2">
									<div class="col-3">Nomor Rekening / VA<strong>*</strong></div>
									<div class="col">
										<input type="hidden" name="uid" class="form-control" value="<?=$user['id']; ?>">
										<input type="text" name="norek_va" class="form-control" placeholder="Norek / VA" value="">
									</div>
								</div>
								<div class="row mt-2">
									<div class="col">
										<strong>* Wajib diisi</strong>
									</div>
									<div class="col">
										<button type="submit" class="btn btn-warning text-dark float-right ml-2">Simpan</button>
										<a href="#" data-dismiss="modal" aria-label="Close" class="btn btn-secondary float-right">Batal</a>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>