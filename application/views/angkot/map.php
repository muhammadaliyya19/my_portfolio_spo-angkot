<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Pakai Tracker Lokasi</title> -->
  <!-- Pointer events polyfill for old browsers, see https://caniuse.com/#feat=pointer -->
  <script src="https://unpkg.com/elm-pep"></script>
  <style type="text/css">
    .map {
      width: 100%;
      height:400px;
    }
  </style>
</head>
<body>
  <div id="map" class="map my-5"></div>
  <div id="info" style="display: none;"></div>
  <label for="track">
    track position
    <input id="track" type="checkbox"/>
  </label>
  <p>
    position accuracy : <code id="accuracy"></code>&nbsp;&nbsp;
    altitude : <code id="altitude"></code>&nbsp;&nbsp;
    altitude accuracy : <code id="altitudeAccuracy"></code>&nbsp;&nbsp;
    heading : <code id="heading"></code>&nbsp;&nbsp;
    speed : <code id="speed"></code>
  </p>
  <!-- <script src="js/ol.js"></script> -->
  <!-- <script src="js/main.js"></script> -->
  <script src="<?= base_url('assets/ol/js/ol.js'); ?>"></script>
  <!-- <script src="<?= base_url('assets/ol/js/main.js'); ?>"></script> -->
  <script src="<?= base_url('assets/js/olscript.js'); ?>"></script>
</body>
</html>