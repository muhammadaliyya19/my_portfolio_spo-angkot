<div class="mt-5 pt-4"></div>
<!-- CONTENT -->
<!-- Start home-about Area -->
<section class="home-about-area" id="home">
	<div class="container">
		<div class="card m-3">
			<div class="card-body">
				<div class="row">
					<div class="col-lg-4">
						<div class="card card-success card-outline" style="height: 100%;">
							<div class="card-body box-profile">
								<div class="text-center">
								<img class="profile-user-img img-fluid thumbnail"
									src="<?=base_url(); ?>assets/img/fav/Network-Profile.png"
									alt="Angkot picture">
								</div>

								<h3 class="profile-username text-center"><?=$thisAngkot['nama']; ?></h3>

								<p class="text-muted text-center"><?=$thisAngkot['nopol']; ?></p>

								<ul class="list-group list-group-unbordered mb-3">
									<li class="list-group-item">
										<b>Driver</b> <a class="float-right"><?=$thisAngkot['sopir']; ?></a>
									</li>
									<li class="list-group-item">
										<b>Last Updated</b> <a class="float-right"><?=$thisAngkot['updated_at']; ?></a>
									</li>
									<li class="list-group-item">
										<b>Join Date</b> <a class="float-right"><?=$thisAngkot['created_at']; ?></a>
									</li>
								</ul>
							</div>
							<!-- /.card-body -->
						</div>
					</div>
					<div class="col-lg-8">
						<div class="card">
							<div class="card-header">
								Location
							</div>
							<div class="card-body">
								<h3>Disini nanti lokasi GPS angkotnya dan posisi pengguna</h3>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>	
</section>
