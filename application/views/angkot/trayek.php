<div class="mt-5 pt-4"></div>
<!-- CONTENT -->
<!-- Start home-about Area -->
<section class="home-about-area" id="home">
	<div class="container">
		<div class="card m-3">
			<div class="card-body">
				<div class="" id="Trayek">	        			
					<div class="row align-items-center">
						<div class="col-12 text-center">
							<h3>Informasi Trayek</h3>
						</div>
						<div class="col-12">
							<div class=" table-responsive">								
								<table id="tabelTrayek" class="table">
									<thead>
										<tr>
											<th scope="col">No.</th>
											<th scope="col">Kode</th>
											<th scope="col">Warna Angkot</th>
											<th scope="col">Jurusan</th>
											<th scope="col">Jarak Rute</th>
											<th scope="col">Action</th>
											<!-- <th scope="col">Aksi</th> -->
										</tr>
									</thead>
									<tfoot>
										<tr>
											<th scope="col">No.</th>
											<th scope="col">Kode</th>
											<th scope="col">Warna Angkot</th>
											<th scope="col">Rute</th>
											<th scope="col">Jarak Rute</th>
											<th scope="col">Action</th>
											<!-- <th scope="col">Aksi</th> -->
										</tr>
									</tfoot>
									<tbody>
										<?php $i = 1; foreach ($trayek as $t): ?>
										<tr>
											<th scope="row"><?=$i; ?></th>
											<td>
												<a href="#" class="cekTrayek" data-toggle="modal" data-target="#modal-detail-trayek" data-id="<?=$t['id_trayek']; ?>">
													<?=$t['kode_trayek'] ?>
												</a>
											</td>
											<td>
												<a href="#" class="cekTrayek" data-toggle="modal" data-target="#modal-detail-trayek" data-id="<?=$t['id_trayek']; ?>">
													<?=$t['warna_angkot'] ?>
												</td>
											</a>
											<td>
												<a href="#" class="cekTrayek" data-toggle="modal" data-target="#modal-detail-trayek" data-id="<?=$t['id_trayek']; ?>">
													<?=$t['jurusan'] ?>
												</a>
											</td>
											<td>
												<a href="#" class="cekTrayek" data-toggle="modal" data-target="#modal-detail-trayek" data-id="<?=$t['id_trayek']; ?>">
													<?=$t['jarak'] ?>
												</a>
											</td>
											<td>												
												<a href="#" class="btn btn-primary btn-icon-split cekTrayek" data-toggle="modal" data-target="#modal-detail-trayek" data-id="<?=$t['id_trayek']; ?>">
													<span class="icon"><i class="fa fa-eye"></i></span><span class="text"> Details</span></a>
												</td>
											</tr>
											<?php $i++; endforeach; ?>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>					
				</div>

			</div>
		</div>
	</div>	
	<!-- MODAL DIALOG Detail trayek -->
	<div class="modal fade" id="modal-detail-trayek">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title">Detail Trayek</h4>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body modal-evidence">
					<div class="card" style="height: 100%;">
						<div class="card-header">
							<span class="h5 nama_trayek">Nama Trayek</span>
							<span class="h5 float-right jarak_trayek">Jarak Trayek (Km)</span>
						</div>
						<div class="card-body">
							<h5 class="card-subtitle mb-2 text-muted text-center">Rute Trayek</h5>
							<div id="holder_rute">
								<ol class="list-group" style="list-style-type: decimal; list-style-position: inside;">
									<li class="list-group-item">Jl 1</li>
									<li class="list-group-item">Jl 2</li>
									<li class="list-group-item">Jl 3</li>
								</ol>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
