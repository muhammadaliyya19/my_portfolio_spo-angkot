<!-- CONTENT -->
<!-- Start home-about Area -->
<section>
<div class="mt-5 pt-4">
	<div class="container">
		<div class="card m-3">
			<div class="card-body">
				<div class="row">
					<div class="col-8">
						<span class="h3" id="viewaround_btn"> Pencarian Angkot </span>
					</div>
					<div class="col-4">
						<?php if($user['role'] != 2): ?>
						<div class="row">
							<div class="col-6">
								<select class="form-control" id="titik_naik">
									<option selected="" value="null">-- Titik naik --</option>
									<?php foreach($rute as $r): ?>
										<option value="<?=$r['id_jalan'] ?>"><?=$r['nama_jalan']; ?></option>
									<?php endforeach; ?>
								</select>
							</div>
							<div class="col-6">
								<select id="titik_turun" class="form-control">
									<option selected="" value="null">-- Titik turun --</option>
									<?php foreach($rute as $r): ?>
										<option value="<?=$r['id_jalan'] ?>"><?=$r['nama_jalan']; ?></option>
									<?php endforeach; ?>
								</select>
							</div>
						</div>
					<?php endif; ?>
					</div>
				</div>
				<!-- 2nd maps, lacak angkot -->
				<div class="" id="viewaround">
					<div id="holderMap" style="">
						<div class="card mt-3" style="height:100%; width:100%; position:relative;">
							<div class="row align-items-center">
								<div class="col-lg-8">
									<div id="map" style="height:400px; width:100%;"></div>
									<div id="info" style="display: none;"></div>
									<label for="track">
										<?=($user['role'] == 2)? 'Lacak & Update Posisi Saya' : 'Lacak Posisi Saya' ?>
										<input id="track" type="checkbox" data-idUser="<?=$user['id']; ?>" data-roleUser="<?=$user['role']; ?>"/>
									</label>									
								</div>
								<div class="col-lg-4" style="overflow-y:scroll; height: 400px; position:relative;">
									<h5>List Angkot</h5>
									<table class="table table-bordered mt-2" id="list_angkot">
										<thead>
											<tr>
												<th>No</th>
												<th>Kode</th>
												<th>Nopol</th>
												<th>Action</th>
											</tr>
										</thead>
										<tbody id="holder_list_angkot">
											<?php $i = 1; foreach($angkot as $a): ?>
											<tr>
												<td><?=$i; ?></td>
												<td><?=$a['kode_trayek']; ?></td>
												<td><?=$a['nopol']; ?></td>
												<td>
													<a href="#" data-angkot="<?=$a['id']; ?>" data-xangkot="<?=$a['x_pos']; ?>" data-yangkot="<?=$a['y_pos']; ?>" class="btn btn-sm btn-outline-success lokasi_angkot"><i class="fas fa-map-marker-alt"></i></a>
													<a href="#" data-angkot="<?=$a['id']; ?>" class="btn btn-sm btn-outline-primary detail_angkot" data-toggle="modal" data-target="#modal-detail-angkot"><i class="fas fa-eye"></i></a>
												</td>
											</tr>
											<?php $i++; endforeach; ?>
										</tbody>
									</table>
								</div>
							</div>
							<?php if($user['role'] != 2): ?>
							<div class="row">
								<div class="col-lg-8"></div>
								<div class="col-lg-4 col-sm-12">
									<a href="<?=base_url('pages/scan/'); ?>" class="btn btn-warning text-dark" style="width:100%;"> Scan QR </a>
								</div>
							</div>						
							<?php endif; ?>
						</div>
					</div>
				</div>
			</div>			
		</div>
	</div>
</div>	

<!-- MODAL DETAIL ANGKOT -->
<div class="modal fade" id="modal-detail-angkot">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Detail Data Angkot</h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body modal-evidence">
				<div class="card" style="height: 100%;">
					<div class="card-header">
						<div class="row">
							<div class="col-6"><span class="h5 nopol_angkot">Nopol Angkot</span></div>
							<div class="col-6"><span class="h5 display-inline float-right text-center warna_angkot">Warna Angkot</span></div>
						</div>
					</div>
					<div class="card-body">
						<div class="text-center "><span class="h5 jurusan_angkot">Jurusan Angkot</span></div>
						<div class="text-center">
							<img class="profile-user-img img-fluid img-circle"
							src="<?= base_url('assets/img/fav/ang.jpg'); ?>"
							alt="Angkot picture" style="height:200px;">
						</div>
						<h5 class="card-subtitle mb-2 mt-1 text-muted text-center">Rute Angkot</h5>
						<div id="holder_rute">
							<ol class="list-group" style="list-style-type: decimal; list-style-position: inside;">
								<li class="list-group-item">Jl 1</li>
								<li class="list-group-item">Jl 2</li>
								<li class="list-group-item">Jl 3</li>
							</ol>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</section>

<!-- 
<script type="text/javascript" src="<?=base_url('/assets/'); ?>js/instascan.min.js"></script>			
<script src="<?=base_url('/assets/'); ?>js/vendor/jquery-2.2.4.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="<?=base_url('/assets/'); ?>js/vendor/bootstrap.min.js"></script>		
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-p34f1UUtsS3wqzfto5wAAmdvj+osOnFyQFpp4Ua3gs/ZVWx6oOypYoCJhGGScy+8" crossorigin="anonymous"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBhOdIF3Y9382fqJYt5I_sswSrEw5eihAA"></script>
<script src="<?=base_url('/assets/'); ?>js/easing.min.js"></script>			
<script src="<?=base_url('/assets/'); ?>js/hoverIntent.js"></script>
<script src="<?=base_url('/assets/'); ?>js/superfish.min.js"></script>	
<script src="<?=base_url('/assets/'); ?>js/jquery.ajaxchimp.min.js"></script>
<script src="<?=base_url('/assets/'); ?>js/jquery.magnific-popup.min.js"></script>	
<script src="<?=base_url('/assets/'); ?>js/jquery-ui.js"></script>								
<script src="<?=base_url('/assets/'); ?>js/jquery.nice-select.min.js"></script>							
<script src="<?=base_url('/assets/'); ?>js/mail-script.js"></script>	
<script src="<?=base_url('/assets/'); ?>js/main_taxi.js"></script>	
<script type="text/javascript" src="<?=base_url('/assets/'); ?>js/payscript.js"></script>	 -->