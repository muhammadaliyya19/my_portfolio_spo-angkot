<div class="mt-5 pt-4"></div>
<!-- CONTENT -->
<!-- Start home-about Area -->
<section class="home-about-area" id="home">
	<div class="container">
		<div class="card m-3">
			<div class="card-body">
				<div class="row">					
					<div class="col-lg-12">
						<div class="card">
							<div class="card-header">
								Lokasi Angkot Trayek <?=$thisTrayek['nama_trayek']; ?>
							</div>
							<div class="card-body">
								<h3>Disini nanti lokasi GPS angkotnya dan posisi pengguna</h3>
								<p>Barangkali juga bisa diberikan titik awal trayek dan titik akhirnya, dengan marker yang berbeda-beda</p>
							</div>
						</div>
					</div>
					<div class="col-lg-12 mt-3">
						<div class="card">
							<div class="card-header">
								Daftar Angkot Trayek <?=$thisTrayek['nama_trayek']; ?>
							</div>
							<div class="card-body">
								<table class="table table-striped">
									<thead>
										<tr>
											<th scope="col">No.</th>
											<th scope="col">Gambar</th>
											<th scope="col">Nopol</th>
											<th scope="col">Trayek</th>
											<th scope="col">Rute</th>
											<th scope="col">Aksi</th>
										</tr>
									</thead>
									<tfoot>
										<tr>
											<th scope="col">No.</th>
											<th scope="col">Gambar</th>
											<th scope="col">Nopol</th>
											<th scope="col">Trayek</th>
											<th scope="col">Rute</th>
											<th scope="col">Aksi</th>
										</tr>
									</tfoot>
									<tbody>
										<?php $i = 1; foreach ($thisTrayekAngkot as $a): ?>
										<tr>
											<th scope="row"><?=$i; ?></th>
											<td><?=$a['gambar']; ?></td>
											<td><?=$a['nopol']; ?></td>
											<td><?=$a['kode_trayek']; ?></td>
											<td><?=$a['kode_trayek']; ?></td>
											<td>												
												<a href="<?=base_url('angkot/detail/'.$a['id']); ?>" class="btn btn-primary btn-icon-split"><span class="icon"><i class="fa fa-eye"></i></span><span class="text"> Details</span></a>
											</td>
										</tr>
										<?php $i++; endforeach; ?>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>	
</section>
