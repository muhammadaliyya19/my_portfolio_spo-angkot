<!DOCTYPE html>
<html lang="zxx" class="no-js">
<head>
	<!-- Mobile Specific Meta -->
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<!-- Favicon-->
	<link rel="shortcut icon" href="<?= base_url('assets/img/fav.png'); ?>">
	<!-- Author Meta -->
	<meta name="author" content="colorlib">
	<!-- Meta Description -->
	<meta name="description" content="">
	<!-- Meta Keyword -->
	<meta name="keywords" content="">
	<!-- meta character set -->
	<meta charset="UTF-8">
	<!-- Site Title -->
	<title><?=$title; ?></title>
	<link rel="icon" type="image/png" href="<?= base_url();?>/assets/img/fav/favicon.ico">  

	<link href="https://fonts.googleapis.com/css?family=Poppins:100,200,400,300,500,600,700" rel="stylesheet"> 

	<script src="https://use.fontawesome.com/releases/v5.15.3/js/all.js" crossorigin="anonymous"></script>
	<!-- Google fonts-->
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.25/css/jquery.dataTables.css">

	<link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css" />
	<link href="https://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet" type="text/css" />	
			<!--
			CSS
			============================================= -->
			<link rel="stylesheet" href="<?= base_url('assets/css/linearicons.css'); ?>">
			<link rel="stylesheet" href="<?= base_url('assets/css/font-awesome.min.css'); ?>">
			<link rel="stylesheet" href="<?= base_url('assets/css/bootstrap.css'); ?>">
			<link rel="stylesheet" href="<?= base_url('assets/css/magnific-popup.css'); ?>">
			<link rel="stylesheet" href="<?= base_url('assets/css/nice-select.css'); ?>">
			<link rel="stylesheet" href="<?= base_url('assets/css/animate.min.css'); ?>">
			<link rel="stylesheet" href="<?= base_url('assets/css/jquery-ui.css'); ?>">
			<link rel="stylesheet" href="<?= base_url('assets/css/main.css'); ?>">

		</head>
		<body style="height: 100%;">	
			<?php if(!empty($user)): ?>
				<header id="header">
					<div class="header-top">
					</div>				
					<div class="container main-menu">						
						<div class="row align-items-center justify-content-between d-flex">
							<a href="<?=base_url();?>"><img src="<?= base_url();?>/assets/img/logo.png" alt="" title="Logos"/></a>
							<nav id="navbar">
								<ul class="nav-menu">
									<li class="menu-active"><a href="<?= base_url();?>"><i class="fas fa-home fa-2x"></i></a></li>
							
									<!-- <li><a href="<?= base_url('pages/angkot');?>">Angkot & Trayek</a></li> -->
									<!-- <li><a href="<?= base_url('pages/scan');?>">Payment</a></li> -->
									<!-- <li><a href="<?= ($user['role'] == '1')? base_url('user/profile') : base_url('driver/profile');?>">Profile</a></li> -->
									<li><a href="<?= base_url('auth/logout');?>"><i class="fas fa-sign-out-alt fa-2x"></i></a></li>
								</ul>
							</nav><!-- #nav-menu-container -->		
						</div>
					</div>
				</header><!-- #header -->
			<?php else: ?>
				<header style="background:black;">
					<div class="header-top">
					</div>
					<div class="container main-menu" style="display:block; position:relative; background:black;">						
						<div class="row align-items-center justify-content-between d-flex">
							<a href="<?=base_url();?>"><img src="<?= base_url();?>/assets/img/logo.png" alt="" title="Logos"/></a>
							<nav id="navbar">
								<ul class="nav-menu">
									<li class="menu-active"><a href="<?= base_url();?>"><i class="fas fa-home fa-2x"></i></a></li>
								</ul>
							</nav><!-- #nav-menu-container -->		
						</div>
					</div>				
				</header>
			<?php endif; ?>