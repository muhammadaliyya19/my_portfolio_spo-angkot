<!-- start footer Area -->		
<footer class="footer-area p-0 m-0" style="display:flex; flex: 1 1 auto; height:200px;">
	<div class="container">
		<div class="row">
			<p class="mt-5 pt-4 mx-auto footer-text col-lg-12">
				<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
				Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="fa fa-heart-o" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
				<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
			</p>											
		</div>
	</div>
	<img class="footer-bottom" src="<?=base_url('/assets/'); ?>img/footer-bottom.png" alt="">
</footer>	
<!-- End footer Area -->	

<script src="<?=base_url('assets/'); ?>/js/vendor/jquery-2.2.4.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="<?=base_url('assets/'); ?>/js/vendor/bootstrap.min.js"></script>			
<script src="<?=base_url('assets/'); ?>/js/easing.min.js"></script>			
<script src="<?=base_url('assets/'); ?>/js/hoverIntent.js"></script>
<script src="<?=base_url('assets/'); ?>/js/superfish.min.js"></script>	
<script src="<?=base_url('assets/'); ?>/js/jquery.ajaxchimp.min.js"></script>
<script src="<?=base_url('assets/'); ?>/js/jquery.magnific-popup.min.js"></script>	
<script src="<?=base_url('assets/'); ?>/js/jquery-ui.js"></script>								
<script src="<?=base_url('assets/'); ?>/js/jquery.nice-select.min.js"></script>							
<script src="<?=base_url('assets/'); ?>/js/mail-script.js"></script>	
<script src="<?=base_url('assets/'); ?>/js/main_taxi.js"></script>
<!-- OPENLAYERS Script -->
<?php if ($title == 'Info Angkot'): ?>	
<!-- <script src="<?= base_url('assets/js/olscript.js'); ?>"></script> -->
<script src="<?= base_url('assets/ol/js/ol.js'); ?>"></script>
<script src="<?= base_url('assets/ol/js/main.js'); ?>"></script>
<?php endif ?>
<!-- MY OWN CUSTOM SCRIPT -->
<script src="<?=base_url('assets/js/script.js'); ?>"></script>
</body>
</html>