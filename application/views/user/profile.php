<div class="mt-5 pt-4"></div>
<!-- CONTENT -->
<!-- Start home-about Area -->
<section class="home-about-area" id="home">
	<div class="container">
		<div class="card m-3">
			<div class="card-body">
				<div class="row">
					<div class="col-12">
						<?php echo $this->session->flashdata('message'); ?>
					</div>
				</div>
				<ul class="nav" role="tablist">
					<li class="active">
						<a href="#profile" class="mr-3 btn btn-warning" aria-controller="profile" role="tab" data-toggle="tab"> Profile </a>
					</li>
					<li>
						<a href="#payment" class="mr-3 btn btn-warning" aria-controller="payment" role="tab" data-toggle="tab"> Payment </a>
					</li>					
					<li class="">
						<a href="#history" class="mr-3 btn btn-warning" aria-controller="history" role="tab" data-toggle="tab"> History </a>
					</li>
				</ul>
				<div class="tab-content">				
					<!-- 1st Data diri/profile -->
					<div class="tab-pane active" id="profile">
						<div class="row align-items-center">
							<div class="col-lg-12">
								<div class="row my-2">
									<div class="col-lg-12 mb-2">
										<div class="card card-success card-outline" style="height: 100%;">
											<div class="card-body box-profile">
												<div class="text-center">
													<img class="profile-user-img img-fluid img-circle"
													src="<?=base_url(); ?>assets/img/fav_profile.png"
													alt="Profile picture" style="height:200px;">
												</div>

												<h3 class="profile-username text-center mt-2">
													<?=$user['name']; ?>
													<a href="#" data-toggle="modal" data-target="#modal-profil" data-area="<?php ?>" data-id="<?php ?>">
														<i class="fa fa-edit"></i>
													</a>
												</h3>

												<p class="text-muted text-center"><?=$user_role; ?></p>

												<ul class="list-group list-group-unbordered mb-3">
													<li class="list-group-item">
														<b>Email</b> <a class="float-right"><?=$user['email']; ?></a>
													</li>
													<li class="list-group-item">
														<b>Address</b> <a class="float-right">
															<?=($user['address'] != '')? $user['address'] : '<span class="text-danger">Mohon lengkapi data!</span>'; ?>
														</a>
													</li>
													<li class="list-group-item">
														<b>Phone</b> <a class="float-right">
															<?=($user['phone'] != '')? $user['address'] : '<span class="text-danger">Mohon lengkapi data!</span>'; ?>
														</a>
													</li>
													<li class="list-group-item">
														<b>Last Updated</b> <a class="float-right"><?=$user['updated_at']; ?></a>
													</li>
													<li class="list-group-item">
														<b>Join Date</b> <a class="float-right"><?=$user['created_at']; ?></a>
													</li>
												</ul>
											</div>
											<!-- /.card-body -->
										</div>
									</div>
									<!-- PLACEHOLDER EDIT PROFILE -->
									<div class="col">
										
									</div>								
								</div>
							</div>							
						</div>				
					</div>				
					<!-- 2nd Riwayat pembayaran -->
					<div class="tab-pane mt-3" id="history">
						<div class="row align-items-center">
							<div class="col-lg-12">
								<div class="card card-success card-outline" style="overflow-y:scroll;">
									<table class="table table-striped">
										<thead>
											<tr>
												<th scope="col">No.</th>
												<th scope="col">Tanggal</th>
												<th scope="col">Jurusan</th>
												<th scope="col">Nopol Angkot</th>
												<th scope="col">Driver</th>
												<th scope="col">Biaya</th>
											</tr>
										</thead>
										<tfoot>
											<tr>
												<th scope="col">No.</th>
												<th scope="col">Tanggal</th>
												<th scope="col">Jurusan</th>
												<th scope="col">Nopol Angkot</th>
												<th scope="col">Driver</th>
												<th scope="col">Biaya</th>
											</tr>
										</tfoot>
										<tbody>
											<?php $i = 1; foreach ($riwayat as $r): ?>
											<tr>
												<td><?=$i; ?></td>
												<td><?=$r['created_at']; ?></td>
												<td><?=$r['jurusan']; ?></td>
												<td><?=$r['nopol']; ?></td>
												<td><?=$r['name']; ?></td>
												<td>Rp. <?=$r['biaya']; ?>,-</td>
											</tr>
											<?php $i++; endforeach; ?>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
					<!-- 3rd Pengaturan pembayaran -->
					<div class="tab-pane mt-3" id="payment">
						<div class="row align-items-center">
							<div class="col-lg-12">
								<div class="row">
									<div class="col-lg-12 col-md-12 col-xs-12">
										<!--  -->
										<div class="row my-2">
											<div class="col-lg-12">
												<div class="card card-success card-outline" style="height: 100%;">
													<div class="card-body box-profile">
														<div class="text-center">
															<img src="<?=$dir;?>" class="thumbnail" alt="Angkot picture" style="height:200px;">
														</div>
														<h3 class="profile-username text-center mt-2">
															Informasi Account
															<a href="#" data-toggle="modal" data-target="#modal-akun" data-area="<?php ?>" data-id="<?php ?>">
																<i class="fa fa-edit"></i>
															</a>
														</h3>

														<p class="text-muted text-center"><?=$user['name']; ?></p>

														<ul class="list-group list-group-unbordered mb-3">
															<li class="list-group-item">
																<b>QR Code</b> <a class="float-right"><?=$account['qris_code']; ?></a>
															</li>
															<li class="list-group-item">
																<b>Saldo</b> <a class="float-right"><?=$account['balance']; ?></a>
															</li>
															<li class="list-group-item">
																<b>Last Transaction</b> <a class="float-right"><?=$account['updated_at']; ?></a>
															</li>
															<li class="list-group-item">
																<b>Last Updated</b> <a class="float-right"><?=$account['updated_at']; ?></a>
															</li>									
														</ul>
													</div>
													<!-- /.card-body -->
												</div>
											</div>									
										</div>
										<!--  -->										
									</div>
								</div>
							</div>
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>
</div>	

<!-- MODAL DIALOG PROFIL -->
<div class="modal fade" id="modal-profil">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Edit Profil</h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body modal-evidence">
				<div class="card" style="height: 100%;">
					<div class="card-body">
						<form action="<?=base_url('user/edit_profile'); ?>" method="post">
							<div class="row mt-2">
								<div class="col-3">Nama<strong>*</strong></div>
								<div class="col">
									<input type="hidden" name="id_user" class="form-control" value="<?=$user['id']; ?>">
									<input type="text" name="name" class="form-control" placeholder="Nama..." value="<?=$user['name']; ?>">
								</div>
							</div>
							<div class="row mt-2">
								<div class="col-3">E-mail<strong>*</strong></div>
								<div class="col">
									<input type="email" name="email" class="form-control" placeholder="E-mail..." value="<?=$user['email']; ?>">
								</div>
							</div>
							<div class="row mt-2">
								<div class="col-3">Phone<strong>*</strong></div>
								<div class="col">
									<input type="number" name="phone" class="form-control" placeholder="Nomor HP..." value="<?=$user['phone']; ?>">
								</div>
							</div>
							<div class="row mt-2">
								<div class="col-3">Alamat<strong>*</strong></div>
								<div class="col">
									<input type="text" name="address" class="form-control" placeholder="Alamat..." value="<?=$user['address']; ?>">
								</div>
							</div>
							<div class="row mt-2">
								<div class="col-3">Password Saat Ini<strong>*</strong></div>
								<div class="col">
									<input type="password" name="old_password" class="form-control" placeholder="Masukkan password ..." >
								</div>
							</div>
							<div class="row mt-2">                
								<div class="col-3">Password Baru</div>
								<div class="col">
									<input type="password" name="password" class="form-control" placeholder="Password baru ...">
								</div>
								<div class="col">
									<input type="password" name="repassword" class="form-control" placeholder="Konfirmasi password ...">
								</div>
							</div>
							<div class="row mt-2">
								<div class="col">
									<strong>* Wajib diisi</strong>
								</div>
								<div class="col">
									<button type="submit" class="btn btn-warning text-dark float-right ml-2">Simpan</button>
									<a href="#" data-dismiss="modal" aria-label="Close" class="btn btn-secondary float-right">Batal</a>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- MODAL ACCOUNT -->
<div class="modal fade" id="modal-akun">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Ubah Rekening Yang Ditautkan</h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="card" style="height: 100%;">
					<div class="card-body">
						<form action="<?=base_url('user/editRek'); ?>" method="post">
							<div class="row mt-2">
								<div class="col-3">Bank / E-Wallet<strong>*</strong></div>
								<div class="col">
									<select name="ewallet" class="form-control">
										<option value="BNI">BNI</option>
										<option value="BRI">BRI</option>
										<option value="Mandiri">Mandiri</option>
										<option value="OVO">OVO</option>
										<option value="Gopay">Gopay</option>
										<option value="Shopee Pay">Shopee Pay</option>
									</select>
								</div>
							</div>
							<div class="row mt-2">
								<div class="col-3">Nomor Rekening / VA<strong>*</strong></div>
								<div class="col">
									<input type="hidden" name="uid" class="form-control" value="<?=$user['id']; ?>">
									<input type="text" name="norek_va" class="form-control" placeholder="Norek / VA" value="">
								</div>
							</div>
							<div class="row mt-2">
								<div class="col">
									<strong>* Wajib diisi</strong>
								</div>
								<div class="col">
									<button type="submit" class="btn btn-warning text-dark float-right ml-2">Simpan</button>
									<a href="#" data-dismiss="modal" aria-label="Close" class="btn btn-secondary float-right">Batal</a>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</section>