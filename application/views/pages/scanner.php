<div class="mt-5 pt-4"></div>
<div class="container justify-content-center">
	<div class="mt-3">
		<div class="row">
			<div class="col-12">
				<?php echo $this->session->flashdata('message'); ?>
			</div>
			<div class="col-12 mt-2">
				<div class="alert alert-warning alert-dismissible fade show" role="alert">
					Silakan lakukan scan QR angkot di titik naik anda!
					<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12 col-md-12">
				<div class="" id="">					
					<?php if ($user['role'] == 1):?>
						<div class="tab-pane fade show active" id="profile" role="tabpanel" aria-labelledby="profile-tab">
							<div class="row mt-3">
								<div class="col-lg-3">
								</div>
								<div class="col-md-12 col-lg-6 mb-3">
									<div class="card">
										<div class="card-header">
											<div class="float-left">
												Camera
											</div>
											<div class="form-check form-switch float-right">
												<input class="form-check-input" type="checkbox" id="camera_check" checked>
												<label class="form-check-label" id="cam_text_status">On</label>
											</div>
										</div>
										<div class="card-body">
											<video id="preview" width="100%">									
											</video>
										</div>
									</div>
								</div>								
								<div class="col-md-12 col-lg-3">
									<!-- <div class="card">
										<div class="card-header">
											Payment Details
										</div>
										<div class="card-body" style="height:auto;"> -->
											<form action="<?= base_url('pages/second_scan'); ?>" method="post" id="form_naik">
												<input type="hidden" required="" name="id_trayek" value="<?=$id_trayek; ?>">
												<input type="hidden" required="" name="qr_from" value="<?=$account['qris_code']; ?>">
												<!-- SCAN RESULT FROM JS -->
												<input type="hidden" required="" name="qr_dest" id="scanres">
												<input type="hidden" required="" name="angkot" id="siangkot">
												<input type="hidden" required="" name="driver" id="driver">
												<!-- Additional fields -->
												<input type="hidden" name="xpos">
												<input type="hidden" name="ypos">
												<!-- <ul class="list-group list-group-unbordered mb-3" style="">
													<li class="list-group-item mt-2">
														<b>QR Code</b> <a class="float-right qrdest"> - </a>
													</li>
													<li class="list-group-item mt-2">
														<b>Angkot</b> <a class="float-right ang"> - </a>
													</li>
													<li class="list-group-item mt-2">
														<b>Driver</b> <a class="float-right driver"> - </a>
													</li>
													<li class="list-group-item mt-2">
														<b>Titik Naik</b> <a class="float-right naik"> - </a>
													</li>

												</ul>	 -->											
											</form>
										<!-- </div> -->
									<!-- </div> -->
								</div>
							</div>
						</div>
					<?php else: ?>		
						<div class="tab-pane fade <?php if($user['role'] == 2){echo "show active";} ?>" id="myaccount" role="tabpanel" aria-labelledby="myaccount">
							<div class="row">
								<div class="col-lg-6 col-md-12 col-xs-12" align="center">
									<img src="<?=$dir;?>" class="thumbnail">
								</div>
								<div class="col-lg-6 col-md-12 col-xs-12">
									<h3 class="profile-username text-center mt-2">Informasi Account</h3>

									<p class="text-muted text-center"><?=$user['name']; ?></p>

									<ul class="list-group list-group-unbordered mb-3">
										<li class="list-group-item">
											<b>QR Code</b> <a class="float-right"><?=$user['qris']; ?></a>
										</li>
										<li class="list-group-item">
											<b>Saldo</b> <a class="float-right"><?=$account['balance']; ?></a>
										</li>
										<li class="list-group-item">
											<b>Last Transaction</b> <a class="float-right"><?=$account['updated_at']; ?></a>
										</li>
										<li class="list-group-item">
											<b>Last Updated</b> <a class="float-right"><?=$account['updated_at']; ?></a>
										</li>									
									</ul>
								</div>
							</div>
						</div>
					<?php endif; ?>		
				</div>
			</div>				
		</div>
	</div>
</div>
<!-- start footer Area -->		
<footer class="footer-area">
	<div class="container">
		<div class="row">
			<p class="mt-5 pb-5 mx-auto footer-text col-lg-12">
				<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
				Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="fa fa-heart-o" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
				<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
			</p>											
		</div>
	</div>
	<img class="footer-bottom" src="<?=base_url('/assets/'); ?>img/footer-bottom.png" alt="">
</footer>	

<script type="text/javascript">
	alert("Silakan scan QR angkot di titik naik anda!");
</script>
<!-- End footer Area -->	
<script type="text/javascript" src="<?=base_url('/assets/'); ?>js/instascan.min.js"></script>			
<script src="<?=base_url('/assets/'); ?>js/vendor/jquery-2.2.4.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="<?=base_url('/assets/'); ?>js/vendor/bootstrap.min.js"></script>		
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-p34f1UUtsS3wqzfto5wAAmdvj+osOnFyQFpp4Ua3gs/ZVWx6oOypYoCJhGGScy+8" crossorigin="anonymous"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBhOdIF3Y9382fqJYt5I_sswSrEw5eihAA"></script>
<script src="<?=base_url('/assets/'); ?>js/easing.min.js"></script>			
<script src="<?=base_url('/assets/'); ?>js/hoverIntent.js"></script>
<script src="<?=base_url('/assets/'); ?>js/superfish.min.js"></script>	
<script src="<?=base_url('/assets/'); ?>js/jquery.ajaxchimp.min.js"></script>
<script src="<?=base_url('/assets/'); ?>js/jquery.magnific-popup.min.js"></script>	
<script src="<?=base_url('/assets/'); ?>js/jquery-ui.js"></script>								
<script src="<?=base_url('/assets/'); ?>js/jquery.nice-select.min.js"></script>							
<script src="<?=base_url('/assets/'); ?>js/mail-script.js"></script>	
<script src="<?=base_url('/assets/'); ?>js/main_taxi.js"></script>	
<?php if($user['role'] != 2): ?>
	<!-- <script src="<?= base_url('assets/js/olscript.js'); ?>"></script> -->
<script src="<?= base_url('assets/ol/js/ol.js'); ?>"></script>
<script src="<?= base_url('assets/ol/js/main.js'); ?>"></script>
	<script type="text/javascript" src="<?=base_url('/assets/'); ?>js/payscript.js"></script>	
<?php endif; ?>
</body>
</html>