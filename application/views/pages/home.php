<div class="fullscreen">	
	<section class="banner-area relative" id="home">
		<div class="overlay overlay-bg"></div>	
		<div class="container">
			<div class="row fullscreen d-flex align-items-center justify-content-center my-0 py-0">		
				<div class="col-lg-12 col-md-12 banner-content">
					<div style="height:75px; position:relative;">&nbsp</div>
					<div class="row mb-3">
						<div class="col text-center">
							<!-- About Section Heading-->
							<h1 class="page-section-heading text-center text-white">Kuber <i class="fas fa-car-alt"></i> <i class="fas fa-bus-alt"></i></h1>							
						</div>	
					</div>		
					<?php if(!empty($user)): ?>
						<a href="<?= base_url('pages/angkot');?>">
							<div class="card m-0 border border-warning" style="background:none;">
								<h2 class="text-center text-warning py-2">
									Cari Angkot
								</h2>											
							</div>				
						</a>
						<a href="<?= base_url('pages/trayek');?>">						
							<div class="card mt-3 border border-warning" style="background:none;">
								<h2 class="text-center text-warning py-2">
									Info Trayek
								</h2>							
							</div>				
						</a>
						<a href="<?= ($user['role'] == '1')? base_url('user/profile') : base_url('driver/profile');?>">
							<div class="card mt-3 border border-warning" style="background:none;">
								<h2 class="text-center text-warning py-2">
									Profil
								</h2>							
							</div>				
						</a>
						<div style="height:100px; position:relative;">&nbsp</div>
					<?php else: ?>		
						<a href="<?= base_url('auth');?>">
							<div class="card m-0 border border-warning" style="background:none;">
								<h1 class="text-center text-warning">
									Masuk
								</h1>				
								<div class="row">
									<div class="col">
										<span class="btn btn-warning btn-block">Log in</span>
									</div>
								</div>				
							</div>				
						</a>
						<a href="<?= base_url('auth/registration');?>">
							<div class="card mt-3 border border-warning" style="background:none;">
								<h1 class="text-center text-warning">
									Daftar
								</h1>
								<div class="row">
									<div class="col">
										<span type="submit" class="btn btn-warning btn-block">Belum punya akun? Daftar!</span>
									</div>
								</div>				
							</div>				
						</a>
						<div style="height:75px; position:relative;">&nbsp</div>
					<?php endif ?>		

				</div>											
			</div>
		</div>					
	</section>
</div>
