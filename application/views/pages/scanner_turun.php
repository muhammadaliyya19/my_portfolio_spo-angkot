<div class="mt-5 pt-4"></div>
<div class="container justify-content-center">
	<div class="mt-3">
		<div class="row">
			<div class="col-12">
				<?php echo $this->session->flashdata('message'); ?>
			</div>
			<div class="col-12 mt-2">
				<div class="alert alert-warning alert-dismissible fade show" role="alert">
					Silakan lakukan scan QR angkot lagi di titik turun dan segera lakukan konfirmasi pembayaran !
					<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12 col-md-12">
				<div class="" id="">					
					<?php if ($user['role'] == 1):?>
						<div class="tab-pane fade show active" id="profile" role="tabpanel" aria-labelledby="profile-tab">
							<div class="row mt-3">
								<div class="col-md-12 col-lg-6 mb-3">
									<div class="card">
										<div class="card-header">
											<div class="float-left">
												Camera
											</div>
											<div class="form-check form-switch float-right">
												<input class="form-check-input" type="checkbox" id="camera_check" checked>
												<label class="form-check-label" id="cam_text_status">On</label>
											</div>
										</div>
										<div class="card-body">
											<video id="preview" width="100%">									
											</video>
										</div>
									</div>
								</div>
								<div class="col-md-12 col-lg-6">
									<div class="card">
										<div class="card-header">
											Payment Details
										</div>
										<div class="card-body" style="height:auto;">
											<form action="<?= base_url('user/doPayment'); ?>" method="post" id="form_turun">
												<input type="hidden" required="" name="qr_from" value="<?=$account['qris_code']; ?>">
												<input type="hidden" required="" name="qr_dest" id="scanres">
												<ul class="list-group list-group-unbordered mb-3" style="">
													<li class="list-group-item mt-2">
														<b>QR Code Angkot</b> <a class="float-right qrdest"> - </a>
													</li>
													<li class="list-group-item mt-2">
														<b>Angkot</b> <a class="float-right ang"> - </a>
													</li>
													<li class="list-group-item mt-2">
														<b>Driver</b> <a class="float-right driver"> - </a>
													</li>
													<!-- <li class="list-group-item mt-2">
														<b>Confirm Angkot</b> <a class="float-right"> <?=$angkot['nopol'] . " - " . $angkot['warna_angkot']  ?> </a>
													</li> -->
												<li class="list-group-item mt-2">
													<div class="row">
														<div class="col-4 float-left"><b>Nominal</b></div>
														<div class="col-8"><input type="number" readonly="" name="amount" placeholder="Rp..." class="float-right form-control" id="bayar" value="0"></div>
													</div>
												</li>								
											</ul>
											<button type="submit" class="btn btn-success" style="width:100%;">Confirm</button>
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
				<?php endif; ?>		
			</div>
		</div>				
	</div>
</div>
</div>
<!-- start footer Area -->		
<footer class="footer-area">
	<div class="container">
		<div class="row">
			<p class="mt-5 pb-5 mx-auto footer-text col-lg-12">
				<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
				Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="fa fa-heart-o" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
				<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
			</p>											
		</div>
	</div>
	<img class="footer-bottom" src="<?=base_url('/assets/'); ?>img/footer-bottom.png" alt="">
</footer>	
<script type="text/javascript">
	alert("Titik naik disimpan! Silakan scan QR angkot lagi di titik turun dan segera lakukan konfirmasi pembayaran!!");
	setTimeout(function(){
	 	alert("Jangan lupa untuk scan ulang QR di titik turun!"); 
	}, 15000);//wait 15 seconds
	setTimeout(function(){
	 	alert("Jangan lupa untuk konfirmasi pembayaran di saat turun!"); 
	}, 20000);//wait 15 seconds	
</script>
<!-- End footer Area -->	
<script type="text/javascript" src="<?=base_url('/assets/'); ?>js/instascan.min.js"></script>			
<script src="<?=base_url('/assets/'); ?>js/vendor/jquery-2.2.4.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="<?=base_url('/assets/'); ?>js/vendor/bootstrap.min.js"></script>		
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-p34f1UUtsS3wqzfto5wAAmdvj+osOnFyQFpp4Ua3gs/ZVWx6oOypYoCJhGGScy+8" crossorigin="anonymous"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBhOdIF3Y9382fqJYt5I_sswSrEw5eihAA"></script>
<script src="<?=base_url('/assets/'); ?>js/easing.min.js"></script>			
<script src="<?=base_url('/assets/'); ?>js/hoverIntent.js"></script>
<script src="<?=base_url('/assets/'); ?>js/superfish.min.js"></script>	
<script src="<?=base_url('/assets/'); ?>js/jquery.ajaxchimp.min.js"></script>
<script src="<?=base_url('/assets/'); ?>js/jquery.magnific-popup.min.js"></script>	
<script src="<?=base_url('/assets/'); ?>js/jquery-ui.js"></script>								
<script src="<?=base_url('/assets/'); ?>js/jquery.nice-select.min.js"></script>							
<script src="<?=base_url('/assets/'); ?>js/mail-script.js"></script>	
<script src="<?=base_url('/assets/'); ?>js/main_taxi.js"></script>	
<?php if($user['role'] != 2): ?>
	<script type="text/javascript" src="<?=base_url('/assets/'); ?>js/payscript.js"></script>	
<?php endif; ?>
</body>
</html>