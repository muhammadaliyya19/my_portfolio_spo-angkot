

<!--  -->
<!--  -->
<!-- start banner Area -->
<section class="banner-area relative" id="home">
	<div class="overlay overlay-bg"></div>	
	<div class="container">
		<div class="row fullscreen d-flex align-items-center justify-content-between">
						<!-- <div class="banner-content col-lg-6 col-md-6 ">
							<h6 class="text-white ">Need a ride? just call</h6>
							<h1 class="text-uppercase">
								911 999 911				
							</h1>
							<p class="pt-10 pb-10 text-white">
								Whether you enjoy city breaks or extended holidays in the sun, you can always improve your travel experiences by staying in a small.
							</p>
							<a href="#" class="primary-btn text-uppercase">Call for taxi</a>
						</div> -->
						<div class="col-lg-12 col-md-12 banner-content">
							<h1 class="text-center text-warning text-uppercase pb-3">Registrasi</h1>
                            <?php echo $this->session->flashdata('message'); ?>
							<form action="<?= base_url('auth/registration'); ?>" method="post">
								<div class="input-group mb-3">
									<input type="text" name="name" placeholder="Nama Lengkap" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Nama Lengkap'" required class="form-control">
								</div>
								<div class="input-group mb-3">
									<input type="email" name="email" placeholder="Alamat Email" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Alamat Email'" required class="form-control">
								</div>
								<div class="input-group mb-3">
									<select name="role" class="form-control">
										<option value="0">-- Select Role --</option>
										<option value="1">Penumpang</option>
										<option value="2">Driver</option>
									</select>
								</div>
								<div class="input-group mb-3">
									<input type="password" name="password" placeholder="Password" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Password'" required class="form-control">
								</div>
								<div class="row">
									<div class="col">
										<button type="submit" class="btn btn-warning btn-block">Buat Akun</button>
									</div>
									<!-- /.col -->
								</div>				
							</form>
						</div>											
					</div>
				</div>					
			</section>
			<!-- End banner Area -->	
