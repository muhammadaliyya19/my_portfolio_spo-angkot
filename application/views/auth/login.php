<div class="fullscreen">	
	<section class="banner-area relative" id="home">
		<div class="overlay overlay-bg"></div>	
		<div class="container">
			<div class="row fullscreen d-flex align-items-center justify-content-center my-0 py-0">		
				<div style="height:100px; position:relative;">&nbsp</div>
				<div class="col-lg-12 col-md-12 banner-content">				
					<h1 class="text-uppercase text-center text-warning pb-3">
						Log in
					</h1>				
					<?php echo $this->session->flashdata('message'); ?>				
					<form action="<?=base_url('auth') ?>" method="POST">
						<div class="input-group mb-3">
							<input type="email" name="email" placeholder="Alamat Email" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Alamat Email'" required class="form-control">
						</div>
						<div class="input-group mb-3">
							<input type="password" name="password" placeholder="Password" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Password'" required class="form-control">
						</div>
						<div class="row">
							<div class="col">
								<button type="submit" class="btn btn-warning btn-block">
									Log in
								</button>
								<div class="text-center mt-2"><span class="h4 text-warning">Belum punya akun? <a href="<?= base_url('auth/registration');?>" class="text-white">Daftar!</a></span></div>
							</div>
							<!-- /.col -->
						</div>				
					</form>
				</div>										
				<div style="height:100px; position:relative;">&nbsp</div>
			</div>
		</div>						
	</section>
</div>
