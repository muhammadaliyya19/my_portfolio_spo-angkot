<?php 
require_once 'assets/phpqrcode/qrlib.php';
class Users_model extends CI_Model
{				
	public function addUser()
	{
		date_default_timezone_set("Asia/Jakarta");
		$now = new DateTime();
		if ($this->input->post('is_validator', true) == null) {
			$data = array(				
				"name"			=> $this->input->post('name', true),
				"email"			=> $this->input->post('email', true),
				"password"		=> password_hash($this->input->post('password'), PASSWORD_DEFAULT),
				// "phone"			=> $this->input->post('phone', true),
				// "address"		=> $this->input->post('address', true),
				"role"			=> $this->input->post('role', true),
				"created_at" 	=> $now->format('Y-m-d H:i:s'),
				"updated_at" 	=> $now->format('Y-m-d H:i:s')
				);			
			//siapkan token
			$token = base64_encode(random_bytes(32));
			$user_token = [
			'email' => $data['email'],
			'token' => $token,
			'date_created' => time()
			];
			$this->db->insert('user', $data);
			$this->db->insert('user_token', $user_token);
			_sendEmail($token, 'verify');
			return 1;
		}else{
			return 0;
		}
	}

	public function deleteUser($id)
	{
		$this->db->delete('user', ['id' => $id]);
		return 1;
	}

	public function updateProfile()
	{
		$thisUser = $this->getUserById($this->input->post('id_user', true));
		date_default_timezone_set("Asia/Jakarta");
		$now = new DateTime();
		$old_password = $this->input->post('old_password', true);
		$new_password = $this->input->post('password', true);
		$re_password = $this->input->post('repassword', true);
		if (password_verify($old_password, $thisUser['password'])) {
			$data = array(				
				"name"			=> $this->input->post('name', true),
				"email"			=> $this->input->post('email', true),
				"password"		=> ($new_password != '' && $new_password == $re_password) ? password_hash($this->input->post('password'), PASSWORD_DEFAULT) : $thisUser['password'],
				"phone"			=> $this->input->post('phone', true),
				"address"		=> $this->input->post('address', true),
				"updated_at" 	=> $now->format('Y-m-d H:i:s')
				);
			// echo "<br><br>Inisialisasi data aman<br><br>";
			// print_r($data['password']);
			// die;
			$this->db->where('id', $this->input->post('id_user'));
			$this->db->update('user', $data);
			return 1;
		}else{
			return 0;
		}
	}

	public function getUserById($id)
	{
		return $this->db->get_where('user' , ['id' => $id])->row_array();
	}

	public function getUserByEmail($email)
	{
		return $this->db->get_where('user' , ['email' => $email])->row_array();
	}	

	public function inisiasiPosisiDriver($id)
	{
		date_default_timezone_set("Asia/Jakarta");
		$now = new DateTime();
		$driver_position = [
		'id_driver' => $id,
		'x_pos' => 0,
		'y_pos' => 0,
		"updated_at" 	=> $now->format('Y-m-d H:i:s')
		];
		$this->db->insert('driver_pos', $driver_position);
	}	

	public function getDriverPositions()
	{
		return $this->db->get('driver_pos')->result_array();
	}

	public function updateDriverPosition()
	{
		date_default_timezone_set("Asia/Jakarta");
		$now = new DateTime();
		$idDriver = $this->input->post('id_driver', true);
		$x_pos = $this->input->post('x_pos', true);
		$y_pos = $this->input->post('y_pos', true);		
		$data = array(				
			"id_driver" 	=> $idDriver,
			"x_pos" 		=> $x_pos,
			"y_pos"		 	=> $y_pos,
			"updated_at" 	=> $now->format('Y-m-d H:i:s')
			);
		$this->db->where('id_driver', $idDriver);
		$this->db->update('driver_pos', $data);
		echo json_encode('success');
	}
}
?>