<?php 
require_once 'assets/phpqrcode/qrlib.php';
class Payment_model extends CI_Model
{				
	// ALL USER ACCOUNT / balance function is here
	public function addUserAccount($user)
	{
		date_default_timezone_set("Asia/Jakarta");
		$now = new DateTime();
		$data = array(				
			"id_user"		=> $user['id'],
			"qris_code"		=> $user['phone'].$user['name'],
			"qris_image"	=> $user['phone'].$user['name'].'.png',
			"balance"		=> 10000,
			"created_at" 	=> $now->format('Y-m-d H:i:s'),
			"updated_at" 	=> $now->format('Y-m-d H:i:s')
			);			
 		$dir = "assets/img/userQR/"; //Nama folder tempat menyimpan file qrcode
 		QRcode::png($data['qris_code'], $dir.$data['qris_image'], QR_ECLEVEL_L, 10);
		$this->db->insert('user_account', $data);
	}

	public function getUserAccountById($id)
	{
		return $this->db->get_where('user_account' , ['id_user' => $id])->row_array();
	}

	public function getUserAccountByQR($qr)
	{
		return $this->db->get_where('user_account' , ['qris_code' => $qr])->row_array();		
	}

	public function Transfer($from, $dest, $amount)
	{
		$from_user = $this->db->get_where('user_account' , ['qris_code' => $from])->row_array();
		$dest_user = $this->db->get_where('user_account' , ['qris_code' => $dest])->row_array();

		// echo "Sebelum transfer <br> From user<br>";
		// print_r($from_user);

		// echo "<br><br>Dest User <br>";
		// print_r($dest_user);

		$fromBalance = $from_user['balance'] - $amount;
		$destBalance = $dest_user['balance'] + $amount;

		// UPDATE FROM Balance
		$this->db->where('qris_code', $from);
		$this->db->update('user_account', ['balance' => $fromBalance]);

		// UPDATE DEST Balance
		$this->db->where('qris_code', $dest);
		$this->db->update('user_account', ['balance' => $destBalance]);

		// $from_user = $this->db->get_where('user_account' , ['qris_code' => $from])->row_array();
		// $dest_user = $this->db->get_where('user_account' , ['qris_code' => $dest])->row_array();

		// echo "<br><br>SETELAH transfer <br> From user<br>";
		// print_r($from_user);

		// echo "<br><br>Dest User <br>";
		// print_r($dest_user);
		// die;
	}
 }
 ?>