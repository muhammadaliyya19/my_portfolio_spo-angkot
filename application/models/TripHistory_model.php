<?php 
class TripHistory_model extends CI_Model
{				
	// ALL USER ACCOUNT / balance function is here
	public function addTripHistory($dest)
	{
		$dest_user = $this->db->get_where('user_account' , ['qris_code' => $dest])->row_array();
		$angkot 	= $this->db->get_where('angkot' , ['sopir' => $dest_user['id_user']])->row_array();
		date_default_timezone_set("Asia/Jakarta");
		$now = new DateTime();
		$data = array(				
			"id_user"		=> $this->session->userdata('user')['id'],
			"id_angkot"		=> $angkot['id'],
			"biaya"			=> $this->input->post('amount', true),
			"created_at" 	=> $now->format('Y-m-d H:i:s')
			);			
		// echo "<br><br><br>";
		// var_dump($data);
		// die;
 		$this->db->insert('riwayat_perjalanan', $data);
 	}

 	public function getTHById($id)
 	{
 		return $this->db->get_where('riwayat_perjalanan' , ['id' => $id])->row_array();
 	}

 	public function getTHByUser($user_id)
 	{
 		$q = 'SELECT angkot.nopol, riwayat_perjalanan.biaya, riwayat_perjalanan.created_at, user.name, trayek.jurusan
 			  FROM riwayat_perjalanan JOIN angkot ON riwayat_perjalanan.id_angkot = angkot.id
 			  JOIN user ON angkot.sopir = user.id
 			  JOIN trayek ON angkot.id_trayek = trayek.id_trayek
 			  WHERE riwayat_perjalanan.id_user = '.$user_id;
 		return $query = $this->db->query($q)->result_array();
 	}

 	public function getTHByDriver($driver_id)
 	{
 		$q = 'SELECT riwayat_perjalanan.biaya, riwayat_perjalanan.created_at
 			  FROM riwayat_perjalanan JOIN angkot ON riwayat_perjalanan.id_angkot = angkot.id
 			  WHERE angkot.sopir = '.$driver_id;
 		return $query = $this->db->query($q)->result_array();	
 	}

 	public function getTotalTHByDriver($driver_id){
 		// $total = 0;
 		$q = 'SELECT sum(riwayat_perjalanan.biaya) as total from riwayat_perjalanan JOIN angkot ON riwayat_perjalanan.id_angkot = angkot.id where angkot.sopir = '.$driver_id  ;
 		return $query = $this->db->query($q)->row_array();
 	}
 }
 ?>