<?php 
require_once 'assets/phpqrcode/qrlib.php';
class Angkot_model extends CI_Model
{				
	public function addAngkot()
	{
		date_default_timezone_set("Asia/Jakarta");
		$now = new DateTime();
		$data = array(				
			"nopol"			=> $this->input->post('nopol', true),
			"id_trayek"	=> $this->input->post('trayek', true),
			"sopir"			=> $this->input->post('sopir', true),
			"created_at" 	=> $now->format('Y-m-d H:i:s'),
			"updated_at" 	=> $now->format('Y-m-d H:i:s')
			);			
		$this->db->insert('angkot', $data);
		return 1;		
	}

	public function deleteAngkot($id)
	{
		$this->db->delete('angkot', ['id' => $id]);
		return 1;
	}

	public function updateAngkot()
	{
		$thisAngkot = $this->getAngkotById($this->input->post('id_angkot', true));
		$current_user = $this->session->userdata('user');
		date_default_timezone_set("Asia/Jakarta");
		$now = new DateTime();
		if ($thisAngkot['sopir'] == $current_user['id']) {
			$data = array(				
				"nopol"			=> $this->input->post('nopol', true),
				"id_trayek"	=> $this->input->post('trayek', true),
				"updated_at" 	=> $now->format('Y-m-d H:i:s')
				);	
			$this->db->where('id', $this->input->post('id_angkot'));
			$this->db->update('angkot', $data);
			return 1;
		}else{
			return 0;
		}
	}

	public function getAllAngkot()
	{
		$this->db->select('*');
		$this->db->from('angkot');
		$this->db->join('trayek', 'angkot.id_trayek = trayek.id_trayek');
		$this->db->join('driver_pos', 'angkot.sopir = driver_pos.id_driver');
		return $query = $this->db->get()->result_array();
	}

	public function getAngkotByDriver($id_driver)
	{
		$this->db->select('*');
		$this->db->from('angkot');
		$this->db->join('trayek', 'angkot.id_trayek = trayek.id_trayek');
		$this->db->where('sopir', $id_driver);
		return $query = $this->db->get()->row_array();
	}

	public function getAngkotById($id)
	{
		return $this->db->get_where('angkot' , ['id' => $id])->row_array();
	}

	public function getAngkotByIdDetails($id)
	{
		$this->db->select('*');
		$this->db->from('angkot');
		$this->db->join('trayek', 'angkot.id_trayek = trayek.id_trayek');
		$this->db->where('angkot.id', $id);
		return $query = $this->db->get()->row_array();
		//return $this->db->get_where('angkot' , ['id' => $id])->row_array();
	}

	public function getAngkotByTrayek($id_trayek)
	{
		$this->db->select('*');
		$this->db->from('angkot');
		$this->db->join('trayek', 'angkot.id_trayek = trayek.id_trayek');
		$this->db->where('angkot.id_trayek', $id_trayek);
		return $query = $this->db->get()->result_array();
		// return $this->db->get_where('angkot' , ['id_trayek' => $id_trayek])->result_array();
	}

	// FUNGSI UNTUK TRAYEK, get only
	public function getAllTrayek()
	{
		return $this->db->get('trayek')->result_array();		
	}

	public function getAllRuteJalan()
	{
		$this->db->select('*');
		$this->db->from('rute_trayek');
		$this->db->join('jalan', 'rute_trayek.id_jalan = jalan.id_jalan');
		return $query = $this->db->get()->result_array();	
	}

	public function getAllJalan()
	{
		$this->db->select('*');
		$this->db->from('jalan');
		return $query = $this->db->get()->result_array();
	}

	public function getTrayekById($id)
	{
		return $this->db->get_where('trayek' , ['id_trayek' => $id])->row_array();		
	}

	public function getRuteByTrayekId($id)
	{
		$this->db->select('*');
		$this->db->from('rute_trayek');
		$this->db->join('jalan', 'rute_trayek.id_jalan = jalan.id_jalan');
		$this->db->where('rute_trayek.id_trayek', $id);
		return $query = $this->db->get()->result_array();
		//return $this->db->get_where('rute_trayek' , ['id_trayek' => $id])->result_array();		
	}

	public function getAngkotByNaikTurun($naik, $turun)
	{
		$query = "SELECT angkot.id as id_angkot, angkot.nopol, trayek.warna_angkot, rute_trayek.id_data, trayek.kode_trayek, trayek.id_trayek, trayek.jurusan, trayek.jarak, rute_trayek.count_biaya, jalan.id_jalan, jalan.nama_jalan, driver_pos.x_pos, driver_pos.y_pos FROM rute_trayek
		LEFT JOIN trayek
		ON rute_trayek.id_trayek = trayek.id_trayek
		LEFT JOIN jalan
		ON rute_trayek.id_jalan = jalan.id_jalan
		JOIN angkot ON rute_trayek.id_trayek = angkot.id_trayek
		JOIN driver_pos ON angkot.sopir = driver_pos.id_driver
		WHERE jalan.id_jalan=$turun;";
		$angkots = $this->db->query($query)->result_array();
		return $angkots;
	}
}
?>