<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Auth extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->library('session');
		$this->load->model('Payment_model');
		$this->load->model('Users_model');		
	}
	public function index()
	{
		$this->form_validation->set_rules('email', 'Username', 'required|trim');
		$this->form_validation->set_rules('password', 'Password', 'required|trim');
		if ($this->form_validation->run() == FALSE) {
			$data['title'] = "Login";
			$this->load->view('layout/header', $data);
			$this->load->view('auth/login');
			$this->load->view('layout/auth_footer');
		} else {
			$this->_login();
		}
	}
	public function registration()
	{		
		$this->form_validation->set_rules('name','Nama','required|trim');
		$this->form_validation->set_rules('role','Role','required');
		$this->form_validation->set_rules('password','Password','required');
		$this->form_validation->set_rules('email','Email','required|trim|valid_email|is_unique[user.email]',[
			'is_unique' => 'This email has already registered'
			]);			
		if ($this->form_validation->run() == FALSE) {
			$data['title'] = "Registrasi";
			$this->load->view('layout/header', $data);
			$this->load->view('auth/regist');
			$this->load->view('layout/auth_footer');
		}else{
			$res = $this->Users_model->addUser();
			if ($res > 0) {				
				$this->session->set_flashdata('message','<div class="alert alert-success alert-dismissible fade show mt-3" role="alert"> Akun terdaftar. Silakan cek email anda untuk aktivasi ! <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
				redirect('auth');
			}else{				
				$this->session->set_flashdata('message','<div class="alert alert-danger alert-dismissible fade show mt-3" role="alert"> Pendaftaran gagal ! <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
				redirect('auth');
			}
		}
	}
	public function verify()
	{
		$email = $this->input->get('email');
		$token = $this->input->get('token');
		$user = $this->db->get_where('user',['email' => $email])->row_array();
		if ($user) {
			$user_token = $this->db->get_where('user_token',['token' => $token])->row_array();
			if ($user_token) {
				$res = $this->deltoken($user_token['id']);
				$this->db->set('is_active', 1);
				$this->db->where('email', $email);
				$this->db->update('user');
				$this->Payment_model->addUserAccount($user);
				if ($user['role'] == 2) {
					$this->Users_model->inisiasiPosisiDriver($user['id']);
				}
				$this->session->set_flashdata('message','<div class="alert alert-success alert-dismissible fade show mt-3" role="alert"> '.$email.' Telah aktif. Silakan login ! <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
				redirect('auth');	
			}else{				
				$this->session->set_flashdata('message','<div class="alert alert-danger alert-dismissible fade show mt-3" role="alert"> Token tidak valid ! <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');	
				redirect('auth');	
			}
		}else{
			$this->session->set_flashdata('message','<div class="alert alert-danger alert-dismissible fade show mt-3" role="alert"> Akun tidak terdaftar ! <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');				
			redirect('auth');
		}
	}
	public function deltoken($tid) {     
		$this->db->where('id' ,$tid);     
		$this->db->delete('user_token');     
		if ($this->db->affected_rows()){         
			return TRUE;    
		}      
	}
	private function _login()
	{
		$email = $this->input->post('email');
		$password = $this->input->post('password');
		$user = $this->Users_model->getUserByEmail($email);		
		if ($user) {
			$user_account = $this->Payment_model->getUserAccountById($user['id']);
			if (password_verify($password, $user['password'])) {
				if ($user['is_active'] == 0) {
					$this->session->set_flashdata('message','<div class="alert alert-danger alert-dismissible fade show mt-3" role="alert"> Akun belum aktif ! Silakan aktivasi akun terlebih dahulu ! <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');					
					redirect('auth');
				}
				$data = [
				'email' 	=> $user['email'],
				'name' 		=> $user['name'],
				'role' 		=> $user['role'],
				'id' 		=> $user['id'],
				'account_id'=> $user_account['id'],
				'qris'		=> $user_account['qris_code']
				];
				$this->session->set_userdata('user', $data);
				$this->session->set_flashdata('message','<div class="alert alert-success alert-dismissible fade show mt-3" role="alert"> Anda berhasil login ! <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');					
				if ($data['role'] == "1") {
					redirect('user');
				}else{
					redirect('driver');
				}
			}else{
				$this->session->set_flashdata('message','<div class="alert alert-danger alert-dismissible fade show mt-3" role="alert"> Password salah ! <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');					
				redirect('auth');
			}
		}else{
			$this->session->set_flashdata('message','<div class="alert alert-danger alert-dismissible fade show mt-3" role="alert"> Akun tidak terdaftar ! <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');	
			
			redirect('auth');
		}
	}
	public function logout()
	{
		session_unset(); 
		session_destroy(); 
		$this->session->set_flashdata('message','<div class="alert alert-success alert-dismissible fade show mt-3" role="alert"> Anda berhasil logout ! <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');							
		redirect('pages');
	}	
}
