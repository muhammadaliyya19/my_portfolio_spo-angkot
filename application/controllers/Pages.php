<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pages extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->library('session');
		$this->load->model('Payment_model');		
		$this->load->model('Users_model');		
		$this->load->model('Angkot_model');		
	}
	public function index()
	{
		$data = [
		'title' => 'Home',
		'user' => $this->session->userdata('user')
		];
		$this->load->view('layout/header', $data);
		$this->load->view('pages/home');
		$this->load->view('layout/footer', $data);
	}
	public function scan($id_trayek = null)
	{
		$titiknaik = (int)$this->input->post('counttitiknaik');
		$titikturun = (int)$this->input->post('counttitikturun');
		$id_angkot = $this->input->post('id_angkot');
		$estimate_cost = abs($titikturun - $titiknaik) * 1500;
		$uid = $this->session->userdata('user')['id'];
		$myQRCode = $this->session->userdata('user')['qris'];
		$myQRCode_Img = $myQRCode.'.png';
		$rute 		= $this->Angkot_model->getRuteByTrayekId($id_trayek);				
		$angkot = $this->Angkot_model->getAngkotByIdDetails($id_angkot);
		$dir = base_url().'assets/img/userQR/'.$myQRCode_Img;								
		$data = [
		'title' => 'Scan Naik',
		'user' => $this->session->userdata('user'),
		'cost' => $estimate_cost,
		'id_trayek' => $id_trayek,
		'angkot' => $angkot,
		'account' => $this->Payment_model->getUserAccountById($uid)
		];
		$this->load->view('layout/header', $data);
		$this->load->view('pages/scanner', $data);
	}

	public function second_scan()
	{
		$estimate_cost = 1500;//abs($titikturun - $titiknaik) * 1500;
		$uid = $this->session->userdata('user')['id'];
		$angkot = $this->input->post('angkot');
		$data = [
		'title' => 'Scan Turun',
		'user' => $this->session->userdata('user'),
		'cost' => $estimate_cost,
		'angkot' => $angkot,
		'account' => $this->Payment_model->getUserAccountById($uid)
		];
		$this->load->view('layout/header', $data);
		$this->load->view('pages/scanner_turun', $data);		
	}

	public function angkot()
	{
		$rute 		= $this->Angkot_model->getAllJalan();
		//var_dump($rute); die;
		$data = [
		'title' => 'Info Angkot',
		'user' => $this->session->userdata('user'),
		'rute' => $rute,
		'angkot' => $this->Angkot_model->getAllAngkot(),
		'trayek' => $this->Angkot_model->getAllTrayek()
		];
		$this->load->view('layout/header', $data);
		$this->load->view('angkot/index', $data);
		$this->load->view('layout/footer', $data);
	}

	public function trayek()
	{
		$data = [
		'title' => 'Info Trayek',
		'user' => $this->session->userdata('user'),
		'angkot' => $this->Angkot_model->getAllAngkot(),
		'trayek' => $this->Angkot_model->getAllTrayek()
		];
		$this->load->view('layout/header', $data);
		$this->load->view('angkot/trayek', $data);
		$this->load->view('layout/footer', $data);
	}

	public function map()
	{
		$data = [
		'title' => 'Info Angkot & Trayek',
		'user' => $this->session->userdata('user')
		];
		$this->load->view('angkot/map');
	}
}
?>

