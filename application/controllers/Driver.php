<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Driver extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->library('session');
		$this->load->model('TripHistory_model');		
		$this->load->model('Payment_model');		
		$this->load->model('Angkot_model');		
		$this->load->model('Users_model');		
	}
	public function index()
	{		
		$data = [
		'title' => 'Home',
		'user' => $this->session->userdata('user')
		];		
		$this->load->view('layout/header', $data);
		$this->load->view('pages/home');
		$this->load->view('layout/footer', $data);
	}
	public function profile()
	{
		$userid = $this->session->userdata('user')['id'];
		$this_user = $this->Users_model->getUserById($userid);
		$trayek_bandung = $this->Angkot_model->getAllTrayek();
		$myQRCode = $this->session->userdata('user')['qris'];
		$myQRCode_Img = $myQRCode.'.png';
		$dir = base_url().'assets/img/userQR/'.$myQRCode_Img;								
		$data = [
		'title' => 'Driver Profile',
		'user' => $this_user,
		'trayek' => $trayek_bandung,
		'revenue' => $this->TripHistory_model->getTHByDriver($userid),
		'total_revenue' => $this->TripHistory_model->getTotalTHByDriver($userid),
		'angkot' => $this->Angkot_model->getAngkotByDriver($userid),
		'dir' => $dir,
		'account' => $this->Payment_model->getUserAccountById($userid),
		'user_role' => ($this->session->userdata('user')['role'] == 1) ? 'Penumpang' : 'Driver'
		];
		$this->load->view('layout/header', $data);
		$this->load->view('driver/profile', $data);
		$this->load->view('layout/footer', $data);						
	}	
	public function edit_profile(){
		$this->form_validation->set_rules('id_user', 'Id User', 'required');
		$this->form_validation->set_rules('name', 'Nama', 'required');
		$this->form_validation->set_rules('email', 'E-mail', 'required');
		$this->form_validation->set_rules('old_password', 'Password Lama', 'required');
		$this->form_validation->set_rules('phone', 'Nomor Telepon', 'required');
		$this->form_validation->set_rules('address', 'Alamat', 'required');	
		if ($this->form_validation->run() == FALSE) {			
			$this->session->set_flashdata('message','<div class="alert alert-danger alert-dismissible fade show" role="alert">Update profil gagal ! Mohon isi formulir dengan benar !<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');						
		}else{
			$res = $this->Users_model->updateProfile();
			if ($res > 0) {
				$this->session->set_flashdata('message','<div class="alert alert-success alert-dismissible fade show" role="alert">Update profil berhasil dilakukan ! <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');						
			}else{
				$this->session->set_flashdata('message','<div class="alert alert-danger alert-dismissible fade show" role="alert">Update profil gagal ! Mohon isi formulir dengan benar !<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');						
			}
		}
		redirect('driver/profile');			
	}
	public function getAllDriverPosition()
	{
		$positions = $this->Users_model->getDriverPositions();
		echo json_encode($positions);
	}
	public function setDriverPosition()
	{
		$cek_role = $this->input->post('role', true);
		if ($cek_role == 2) {
			$this->Users_model->updateDriverPosition();
		}else{
			echo json_encode("anda bukan driver " . $cek_role);
		}
	}
}