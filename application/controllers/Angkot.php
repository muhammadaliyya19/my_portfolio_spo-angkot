<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Angkot extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library('session');
		$this->load->model('Users_model');		
		$this->load->model('Angkot_model');		
	}
	
	public function detail($id)
	{
		$data = [
		'title' => 'Detail Angkot',
		'user' => $this->session->userdata('user'),
		'thisAngkot' => $this->Angkot_model->getAngkotById($id)
		];
		$this->load->view('layout/header', $data);
		$this->load->view('angkot/detail', $data);
		$this->load->view('layout/footer', $data);
	}

	public function trayek($id)
	{
		$thisTrayek = $this->Angkot_model->getTrayekById($id);
		$data = [
		'title' => 'Detail Trayek',
		'user' => $this->session->userdata('user'),
		'thisTrayekAngkot' => $this->Angkot_model->getAngkotByTrayek($thisTrayek['kode_trayek']),
		'thisTrayek' => $thisTrayek
		];
		$this->load->view('layout/header', $data);
		$this->load->view('angkot/detail_trayek', $data);
		$this->load->view('layout/footer', $data);
	}

	public function tambahAngkot()
	{
		$this->form_validation->set_rules('sopir', 'Pengemudi', 'required');
		$this->form_validation->set_rules('nopol', 'Nomor Polisi Kendaraan', 'required');
		$this->form_validation->set_rules('trayek', 'Kode Trayek', 'required');
		if ($this->form_validation->run() == FALSE) {			
			$this->session->set_flashdata('message','<div class="alert alert-danger alert-dismissible fade show" role="alert">Data gagal ditambahkan! Mohon isi formulir degnan benar !<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');						
		}else{
			$res = $this->Angkot_model->addAngkot();					
			if ($res > 0) {
				$this->session->set_flashdata('message','<div class="alert alert-success alert-dismissible fade show" role="alert">Data berhasil disimpan ! <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');						
			}else{
				$this->session->set_flashdata('message','<div class="alert alert-danger alert-dismissible fade show" role="alert">Data gagal disimpan ! Mohon isi formulir dengan benar !<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');						
			}
		}
		redirect('driver/profile');			
	}

	public function editAngkot()
	{
		$this->form_validation->set_rules('id_angkot', 'Id Angkot', 'required');
		$this->form_validation->set_rules('nopol', 'Nomor Polisi Kendaraan', 'required');
		$this->form_validation->set_rules('trayek', 'Kode Trayek', 'required');
		if ($this->form_validation->run() == FALSE) {			
			$this->session->set_flashdata('message','<div class="alert alert-danger alert-dismissible fade show" role="alert">Update data angkot gagal ! Mohon isi formulir degnan benar !<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');						
		}else{
			$res = $this->Angkot_model->updateAngkot();					
			if ($res > 0) {
				$this->session->set_flashdata('message','<div class="alert alert-success alert-dismissible fade show" role="alert">Update data angkot berhasil ! <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');						
			}else{
				$this->session->set_flashdata('message','<div class="alert alert-danger alert-dismissible fade show" role="alert">Update data angkot gagal ! Mohon isi formulir dengan benar !<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
			}
		}
		redirect('driver/profile');			
	}

	public function getTrayekById()
	{
		$id = $this->input->post('id_trayek', true);
		$thisAngkotTrayek = $this->Angkot_model->getTrayekById($id);
		echo json_encode($thisAngkotTrayek);
	}

	public function getAllAngkot()
	{
		$Angkot = $this->Angkot_model->getAllAngkot();
		echo json_encode($Angkot);
	}

	public function getAngkotByTrayek()
	{
		$id = $this->input->post('id_trayek', true);
		$thisAngkotTrayek = $this->Angkot_model->getAngkotByTrayek($id);
		echo json_encode($thisAngkotTrayek);
	}

	public function getAngkotById()
	{
		$id = $this->input->post('id_angkot', true);
		$thisAngkotDetails = $this->Angkot_model->getAngkotByIdDetails($id);
		$rute 		= $this->Angkot_model->getRuteByTrayekId($thisAngkotDetails['id_trayek']);		
		$data = [
			'angkot' => $thisAngkotDetails,
			'rute' => $rute
		];
		echo json_encode($data);
	}

	public function get_trayek_route()
	{
		$id_trayek	= $this->input->post('id_trayek');
		$rute 		= $this->Angkot_model->getRuteByTrayekId($id_trayek);		
		$trayek 	= $this->Angkot_model->getTrayekById($id_trayek);
		$data = [
		'rute'		=> $rute,
		'trayek' 	=> $trayek
		];			
		echo json_encode($data);
	}

	public function getAngkotByNaikTurun()
	{
		$titiknaik = $this->input->post('titik_naik');
		$titikturun = $this->input->post('titik_naik');
		$angkots = $this->Angkot_model->getAngkotByNaikTurun($titiknaik, $titikturun);
		echo json_encode($angkots);
	}
}