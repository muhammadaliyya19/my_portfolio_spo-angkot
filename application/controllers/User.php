<?php
defined('BASEPATH') or exit('No direct script access allowed');
require_once 'assets/phpqrcode/qrlib.php';
class User extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->library('session');
		$this->load->model('Angkot_model');		
		$this->load->model('Payment_model');
		$this->load->model('TripHistory_model');		
		$this->load->model('Users_model');		
	}	
	
	public function index()
	{
		$data = [
		'title' => 'Home',
		'user' => $this->session->userdata('user')
		];
		$this->load->view('layout/header', $data);
		$this->load->view('pages/home');
		$this->load->view('layout/footer', $data);
	}
	
	public function profile()
	{
		$userid 	= $this->session->userdata('user')['id'];
		$this_user 	= $this->Users_model->getUserById($userid);
		$myQRCode = $this->session->userdata('user')['qris'];
		$myQRCode_Img = $myQRCode.'.png';
		$dir = base_url().'assets/img/userQR/'.$myQRCode_Img;								
		$data		= [
		'account' => $this->Payment_model->getUserAccountById($userid),
		'title' => 'User Profile',
		'user' => $this_user,
		
		'dir' => $dir,
		'riwayat' => $this->TripHistory_model->getTHByUser($userid),
		'user_role' => ($this->session->userdata('user')['role'] == 1) ? 'Penumpang' : 'Driver'
		];
		
		$this->load->view('layout/header', $data);
		$this->load->view('user/profile', $data);
		$this->load->view('layout/footer');		
	}
	
	public function doPayment()
	{
		$id_trayek = $this->input->post('id_trayek');
		$amount = $this->input->post('amount');
		$dest = $this->input->post('qr_dest');
		$from = $this->input->post('qr_from');
		$myAccount = $this->Payment_model->getUserAccountByQR($from);
		if ($this->form_validation->run() != FALSE) {			
			echo "<script>alert('Pembayaran gagal,saldo tidak mencukupi!'); window.location.href='".base_url('pages/angkot')."';</script>";
		}else{
			if ($myAccount['balance'] > $amount) {
				$this->Payment_model->Transfer($from, $dest, $amount);
				$this->TripHistory_model->addTripHistory($dest);
				echo "<script>alert('Pembayaran sukses, riwayat perjalanan tersimpan!'); window.location.href='".base_url()."';</script>";			
			}else{
				echo "<script>alert('Pembayaran gagal,saldo tidak mencukupi!'); window.location.href='".base_url('pages/angkot')."';</script>";			
			}
		}					
	}	

	public function edit_profile()
	{
		$this->form_validation->set_rules('address', 'Alamat', 'required');	
		$this->form_validation->set_rules('email', 'E-mail', 'required');
		$this->form_validation->set_rules('id_user', 'Id User', 'required');
		$this->form_validation->set_rules('name', 'Nama', 'required');
		$this->form_validation->set_rules('old_password', 'Password Lama', 'required');
		$this->form_validation->set_rules('phone', 'Nomor Telepon', 'required');
		if ($this->form_validation->run() == FALSE) {			
			$this->session->set_flashdata('message','<div class="alert alert-danger alert-dismissible fade show" role="alert">Update profil gagal ! Mohon isi formulir dengan benar !<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');						
		}else{
			$res = $this->Users_model->updateProfile();
			if ($res > 0) {
				$this->session->set_flashdata('message','<div class="alert alert-success alert-dismissible fade show" role="alert">Update profil berhasil dilakukan ! <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');						
			}else{
				$this->session->set_flashdata('message','<div class="alert alert-danger alert-dismissible fade show" role="alert">Update profil gagal ! Mohon isi formulir dengan benar !<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');		
			}
		}
		redirect('user/profile');			
	}

	public function editRek()
	{		
		$this->session->set_flashdata('message','<div class="alert alert-success alert-dismissible fade show" role="alert">Update rekening berhasil dilakukan ! <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');					
		redirect('user/profile');					
	}

	public function getAccountJson()
	{		
		$inpqr 		= $this->input->post('qris');
		$account 	= $this->Payment_model->getUserAccountByQR($inpqr);		
		$user 		= $this->Users_model->getUserById($account['id_user']);
		$angkot 	= $this->Angkot_model->getAngkotByDriver($user['id']);
		$data = [
		'angkot'	=> $angkot,
		'account' 	=> $account,
		'user' 		=> $user
		];			
		echo json_encode($data);
	}

}
?>